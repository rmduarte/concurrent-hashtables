module HashTable.HashSet where

import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.MVar
import Data.CAS


type Buckets = Array Int (IORef [Int])
type Locks = Array Int (MVar ())

data HTable = TH
	{
	buckets	 :: IORef Buckets,  -- colocar a tabela em um IORef do Bem
	lock	   :: Locks, 			-- agora os locks são um Array com o mesmo tamanho inicial da tabela
	n_elemens :: IORef Int,
	len_tab	 :: IORef Int,
	len_lock :: Int
	}

newHash :: Int -> IO (HTable)
newHash initSize = do
	slots <- replicateM initSize (newIORef [])
	arrayHash <- let arrayHash' = listArray (0,(initSize-1)) slots in arrayHash' `seq` return arrayHash'
	locks <- replicateM initSize (newMVar ())
	lock <- let lock' = listArray (0,(initSize-1)) locks in lock' `seq` return lock'
	buckets <- newIORef arrayHash
	n_elemens <- newIORef 0
	len_tab <- newIORef initSize
	return (TH buckets lock n_elemens len_tab initSize)

-- --------------------- função de inserção do valor ------------------------------

insert :: HTable -> Int -> IO (Bool)
insert table@(TH buckets lock n_elemens len_tab len_lock) value = do
	lockAcquire lock len_lock value 						-- adquire o lock especifico da posição
	hash <- readIORef buckets 								-- le a tabela hash
	tamHash <- readIORef len_tab 							-- le o tamanho da tabela hash
	qtdInser <- readIORef n_elemens							-- le a quantidade de valores inseridos
	let pos_hash = hashfun value tamHash 					-- calcula a posição onde sera inserido o elemento na hash
	slot <- readIORef $ hash ! pos_hash
	if(elem value slot) then do
		lockRelease lock len_lock value
		return False
		else do
			if (threshold qtdInser tamHash) then do
				nl <- let l = value:slot in l `seq` return l
				writeIORef (hash ! pos_hash) nl 		 			-- insere o valor na hash
				incHashInsert n_elemens								-- incrementa o valor de inserções (atomic)
				lockRelease lock len_lock value						-- libera o lock
				return True
				else do
					lockRelease lock len_lock value 						-- libera o lock (motivo: evitar deadlock com outra thread)
					acquireAllLocks lock len_lock 							-- adquire todos os locks da tabela
					new_tamHash <- readIORef len_tab 						-- re verifica o tamanho da tabela para saber se nenhuma outra thread já fez resize
					if (tamHash < new_tamHash) then do
						releaseAllLocks lock len_lock
						insert table value
						else do
							new_hash <- resize hash tamHash
							writeIORef buckets new_hash
							writeIORef len_tab (tamHash * 2)
							releaseAllLocks lock len_lock
							insert table value

-- -------------------------------------------------------------------------------

-- -------------------- função de consulta do valor ------------------------------

contains :: HTable -> Int -> IO (Bool)
contains table@(TH buckets lock n_elemens len_tab len_lock) value = do
	let pos_lock = hashfun value len_lock
	lockAcquire lock len_lock value
	hash <- readIORef buckets
	sizeHash <- readIORef len_tab
	let pos_hash = hashfun value sizeHash
	bucket <- readIORef $ hash ! pos_hash
	if (elem value bucket) then do
		lockRelease lock len_lock value
		return True
		else do
			lockRelease lock len_lock value
			return False
-- -------------------------------------------------------------------------------

-- ------------------- função de remoção do valor --------------------------------
delete :: HTable -> Int -> IO (Bool)
delete table@(TH buckets lock n_elemens len_tab len_lock) value = do
	let pos_lock = hashfun value len_lock
	lockAcquire lock len_lock value
	hash <- readIORef buckets
	sizeHash <- readIORef len_tab
	let pos_hash = hashfun value sizeHash
	slot <- readIORef $ hash ! pos_hash
	if (elem value slot) then do
		nl <- let l = filter (\x -> x /= value) slot in l `seq` return l
		writeIORef (hash! pos_hash) nl
		decHashInsert n_elemens
		lockRelease lock len_lock value
		return True
		else do
			lockRelease lock len_lock value
			return False

-- -------------------------------------------------------------------------------



-- ---------------- Funções auxiliares -------------------------------
acquireAllLocks :: Locks -> Int -> IO ()
acquireAllLocks locks 0 = return ()
acquireAllLocks locks qtdLocks = do
	x <- tryTakeMVar $ locks ! (qtdLocks-1) 				-- tenta adquirir o lock
	case x of
		Just a -> acquireAllLocks locks (qtdLocks-1) 		-- se falhar, tenta novamente
		Nothing -> acquireAllLocks locks (qtdLocks)		-- se conseguir, vai para o proximo lock, tem de ser pego em ordem para evitar DL

releaseAllLocks :: Locks -> Int -> IO ()
releaseAllLocks locks 0 = return () --putMVar (locks ! 0) False
releaseAllLocks locks qtdLocks = do
	putMVar (locks ! (qtdLocks - 1)) ()
	releaseAllLocks locks $ qtdLocks - 1


atomCAS :: Eq a => IORef a -> a -> a -> IO Bool
atomCAS ptr old new =
   atomicModifyIORefCAS ptr (\ cur -> if cur == old
                                   then (new, True)
                                   else (cur, False))

incHashInsert :: IORef Int -> IO Bool
incHashInsert var = do
	old <- readIORef var
	new <- let n = old+1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else incHashInsert var

decHashInsert :: IORef Int -> IO Bool
decHashInsert var = do
	old <- readIORef var
	new <- let n = old-1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else decHashInsert var

lockAcquire :: Locks -> Int -> Int -> IO ()
lockAcquire locks size_locks val = do
	x <- tryTakeMVar $ locks ! (hashfun val size_locks)
	case x of
		Just a -> return ()
		Nothing -> lockAcquire locks size_locks val

lockRelease :: Locks -> Int -> Int -> IO ()
lockRelease locks size_locks val = putMVar (locks ! (hashfun val size_locks)) ()

-- -------------------------------------------------------------------------------------------

hashfun :: Int -> Int -> Int
hashfun val len_tab = mod val len_tab

threshold :: Int -> Int -> Bool
threshold n_elemens sizeTab = fromIntegral n_elemens * 0.75 <= fromIntegral sizeTab

copy_list :: [Int] -> Buckets -> Int -> IO Bool
copy_list [] array1 size_array = return True
copy_list (x:xs) array1 size_array = do
	let array_pos = hashfun x size_array
	list <- readIORef (array1 ! array_pos)
	newList <- let nl = x:list in nl `seq` return nl
	writeIORef (array1 ! array_pos) newList
	copy_list xs array1 size_array

resizeArray :: Buckets -> Buckets -> Int -> Int -> IO Buckets
resizeArray array1 array2 0 new_size = return array2
resizeArray array1 array2 old_size new_size = do
	lista <- readIORef $ array1 ! (old_size-1)
	copy_list lista array2 new_size
	resizeArray array1 array2 (old_size-1) new_size

resize :: Buckets -> Int -> IO Buckets
resize old_array old_size = do
	buckets <- replicateM (old_size*2) (newIORef [])
	arrayHash <- let arrayHash' = listArray (0,(old_size*2)-1) buckets in arrayHash' `seq` return arrayHash'
	new_array <- resizeArray old_array arrayHash old_size (old_size*2)
	return new_array
-- ---------------Implementação do atomCAS ----------------------
