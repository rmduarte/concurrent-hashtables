module HashTable.HashSet where
import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.STM
--import GHC.Conc

type Buckets = Array Int [Int]

data Table = T {
  buckets :: Buckets,
  n_elemens :: Int,
  len_table :: Int
}

data HTable = TH { table :: TVar Table }

newHash :: Int -> STM (HTable)
newHash size = do
	let x = replicate size []
	vetor <- let vetor' = listArray (0,size-1) x in vetor' `seq` return vetor'
	table <- newTVar $ T vetor 0 size
	return (TH table)

insert :: HTable -> Int -> STM Bool
insert tb@(TH table) value = do
  (T buckets n_elemens len_table) <- readTVar table
  if (threshold n_elemens len_table) then do
    let pos = hashfun value len_table
    let l = buckets ! pos
    if (elem value l) then do
      return False
      else do
        newBuckets <- let newBuckets' = buckets // [(pos,value:l)] in newBuckets' `seq` return newBuckets'
        writeTVar table $ T newBuckets (n_elemens + 1) len_table
        return True
    else do
      newBuckets <- resize buckets len_table
      writeTVar table $ T newBuckets n_elemens (len_table*2)
      insert tb value

contains :: HTable -> Int -> STM Bool
contains tb@(TH table) value = do
	hashTable <- readTVar table
	let pos = hashfun value (len_table hashTable)
	let l = ( buckets hashTable) ! pos
	if (elem value l) then do
		return True
		else do
			return False

delete :: HTable -> Int -> STM Bool
delete tb@(TH table) value = do
  (T buckets n_elemens len_table) <- readTVar table
  let pos = hashfun value len_table
  let l = buckets ! pos
  if (elem value l) then do
    newBuckets <- let newBuckets' = buckets // [(pos,filter (\x -> x /= value) l)] in newBuckets' `seq` return newBuckets'
    writeTVar table $ T newBuckets (n_elemens - 1) len_table
    return True
    else do
      return False

hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab

threshold :: Int -> Int -> Bool
threshold n_elemens tamTab = fromIntegral n_elemens * 0.75 <= fromIntegral tamTab

---- ---------------------------------------------------------------------

copy_list :: [Int] -> Buckets -> Int -> STM Buckets
copy_list [] array1 tam_array = return array1
copy_list (x:xs) array1 tam_array = do
	let array_pos = hashfun x tam_array
	let l = array1 ! array_pos
	newArray <- let newArray' = array1 // [(array_pos,x:l)] in newArray' `seq` return newArray'
	--let newArray = array1 // [(array_pos,x:l)]
	copy_list xs newArray tam_array

resizeArray :: Buckets -> Buckets -> Int -> Int -> STM Buckets
resizeArray array1 array2 0 new_tam = return array2
resizeArray array1 array2 old_tam new_tam = do
	let lista = array1 ! (old_tam-1)
	newArray <- copy_list lista array2 new_tam
	resizeArray array1 newArray (old_tam-1) new_tam

resize :: Buckets -> Int -> STM Buckets
resize old_array old_tam = do
	let x = replicate (old_tam*2) []
	newArray <- let newArray' = listArray (0,(old_tam*2)-1) x in newArray' `seq` return newArray'
	--let newArray = listArray (0,(old_tam*2)-1) x
	newHash <- resizeArray old_array newArray old_tam (old_tam*2)
	return newHash
