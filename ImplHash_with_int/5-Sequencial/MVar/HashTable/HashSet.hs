module HashTable.HashSet where
import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.MVar

type Buckets = Array Int [Int]
-- type Ninsert = Int
-- type SizeTab = Int

data HTable = TH
    {
    buckets :: Buckets,
    n_insert :: Int,
    tab_size :: Int
    } deriving(Show)

newHash :: Int -> HTable
newHash size = (TH (listArray (0,size-1) $ replicate size []) 0 size)
--
insert :: HTable -> Int -> (HTable,Bool)
insert table@(TH buckets n_insert tab_size) value | elem value l = (table,False)
                                                  | not(threshold n_insert tab_size) = insert (TH newBuckets n_insert (tab_size*2)) value
                                                  | otherwise = ((TH (buckets // [(pos,value:l)]) (n_insert+1) tab_size),True)
                         where pos = hashfun value tab_size;
                                 l = buckets ! pos;
						newBuckets = resize buckets tab_size

contains :: HTable -> Int -> Bool
contains table@(TH buckets n_insert tab_size) value | elem value $ buckets ! hashfun value tab_size = True
                                                    | otherwise = False

delete :: HTable -> Int -> (HTable,Bool)
delete table@(TH buckets n_insert tab_size) value | elem value l = ( TH (buckets // [(pos,filter (\x -> x /= value) l)]) (n_insert-1) tab_size,True)
                                                  | otherwise = (table,False)
                      where pos = hashfun value tab_size;
                              l = buckets ! pos

hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab

threshold :: Int -> Int -> Bool
threshold n_insert tamTab = fromIntegral n_insert * 0.75 <= fromIntegral tamTab
-- ---- ---------------------------------------------------------------------
copy_list :: [Int] -> Buckets -> Int -> Buckets
copy_list [] array1 size_array = array1
copy_list (x:xs) array1 size_array = copy_list xs (array1 // [(pos,x:l)]) size_array
	where pos = hashfun x size_array;
	        l = array1 ! pos

resizeArray :: Buckets -> Buckets -> Int -> Int -> Buckets
resizeArray array1 array2 0 new_size = array2
resizeArray array1 array2 old_size new_size = resizeArray array1 (copy_list l array2 new_size) (old_size-1) new_size
          where l = array1 ! (old_size-1)

resize :: Buckets -> Int -> Buckets
resize old_array old_size = resizeArray old_array (listArray (0,(old_size*2)-1) (replicate (old_size*2) [])) (old_size) (old_size*2)
