module HashTable.HashSet where

import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.STM

import Data.CAS
import GHC.Conc.Sync

type Buckets = Array Int (TVar [Int])

data HTable = TH 
	{
	buckets	 :: TVar Buckets,  -- colocar a tabela em um IORef do Bem
	n_elemens :: TVar Int,
	lem_tab	 :: TVar Int
	}

newHash :: Int -> STM (HTable)
newHash initSize = do
	slots <- replicateM initSize (newTVar [])
	arrayHash <- let arrayHash' = listArray (0,initSize-1) slots in arrayHash' `seq` return arrayHash'
	buckets <- newTVar arrayHash 
	n_elemens <- newTVar 0
	lem_tab <- newTVar initSize
	return (TH buckets n_elemens lem_tab)


-- --------------------- função de inserção do valor ------------------------------
insert :: HTable -> Int -> STM (Bool)
insert table@(TH buckets n_elemens lem_tab) value = do
	sizeHash <- readTVar lem_tab
	qtdInser <- readTVar n_elemens
	let pos_hash = hashfun value sizeHash
	hash <- readTVar buckets
	slot <- readTVar $ hash ! pos_hash
	if(elem value slot) then do
		return False
		else do
			if(threshold qtdInser sizeHash) then do 							-- verificar se o elemento já está e alterar o fator de carga da hash
				nl <- let l = value:slot in l `seq` return l
				writeTVar (hash ! pos_hash) nl
				writeTVar n_elemens (qtdInser + 1)
				return True
				else do
					new_hash <- resize hash sizeHash
					let pos_hash = hashfun value (sizeHash * 2)
					slot <- readTVar $ new_hash ! pos_hash
					nl <- let l = value:slot in l `seq` return l
					writeTVar (new_hash ! pos_hash) (nl)
					writeTVar buckets new_hash
					writeTVar lem_tab (sizeHash * 2)
					writeTVar n_elemens (qtdInser + 1)
					return True

---- -------------------------------------------------------------------------------

---- -------------------- função de consulta do valor ------------------------------

contains :: HTable -> Int -> STM (Bool)
contains table@(TH buckets n_elemens lem_tab) value = do
	sizeHash <- readTVar lem_tab
	hash <- readTVar buckets
	let pos_hash = hashfun value sizeHash
	slot <- readTVar $ hash ! pos_hash
	if (elem value slot) then do
		return True 
		else do
			return False
---- -------------------------------------------------------------------------------

---- ------------------- função de remoção do valor --------------------------------
delete :: HTable -> Int -> STM (Bool)
delete table@(TH buckets n_elemens lem_tab) value = do
	sizeHash <- readTVar lem_tab
	let pos_hash = hashfun value sizeHash
	hash <- readTVar buckets
	slot <- readTVar $ hash ! pos_hash
	if (elem value slot) then do
		nl <- let l = filter (\x -> x /= value) slot in l `seq` return l
		writeTVar (hash! pos_hash) nl
		qtdInser <- readTVar n_elemens
		writeTVar n_elemens (qtdInser-1)
		return True 
		else do
			return False

---- -------------------------------------------------------------------------------
---- ---------------- Funções auxiliares -------------------------------
hashfun :: Int -> Int -> Int
hashfun val lem_tab = mod val lem_tab

threshold :: Int -> Int -> Bool
threshold n_elemens sizeTab = fromIntegral n_elemens * 0.75 <= fromIntegral sizeTab

copy_list :: [Int] -> Buckets -> Int -> STM Bool
copy_list [] array1 size_array = return True
copy_list (x:xs) array1 size_array = do
	let array_pos = hashfun x size_array
	list <- readTVar (array1 ! array_pos)
	newList <- let nl = x:list in nl `seq` return nl
	writeTVar (array1 ! array_pos) (newList)
	copy_list xs array1 size_array

resizeArray :: Buckets -> Buckets -> Int -> Int -> STM Buckets
resizeArray array1 array2 0 new_size = return array2
resizeArray array1 array2 old_size new_size = do
	lista <- readTVar $ array1 ! (old_size-1)
	if (lista /= []) then do
		copy_list lista array2 new_size
		resizeArray array1 array2 (old_size-1) new_size
		else resizeArray array1 array2 (old_size-1) new_size

resize :: Buckets -> Int -> STM Buckets
resize old_array old_size = do
	buckets <- replicateM (old_size*2) (newTVar [])
	let arrayHash = listArray (0,(old_size*2)-1) buckets
	new_array <- resizeArray old_array arrayHash old_size (old_size*2)
	return new_array
