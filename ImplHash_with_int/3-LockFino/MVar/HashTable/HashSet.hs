module HashTable.HashSet where

import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent
import Control.Concurrent.MVar
import Data.CAS


type Buckets = Array Int (MVar [Int])
type Locks = IORef Buckets

data ThId = ThId ThreadId | Null
	deriving(Eq, Show)

data HTable = TH 
	{
	buckets	 :: Locks,			  		-- Na implementação com lock fino, a tabela é os locks 
	n_elements :: IORef Int, 				-- armazena o número de inserções na tabela hash
	len_tab	 :: IORef Int, 				-- armazena o tamanho da tabela hash
	onGrow	 :: IORef ThId 		-- usado como flag para sinalizar que está em crescimento
	}

newHash :: Int -> IO (HTable)
newHash initSize = do
	slots <- replicateM initSize (newMVar [])
	arrayHash <- let arrayHash' = listArray (0,(initSize-1)) slots in arrayHash' `seq` return arrayHash'
	buckets <- newIORef arrayHash
	n_elements <- newIORef 0
	len_tab <- newIORef initSize
	onGrow <- newIORef Null
	return (TH buckets n_elements len_tab onGrow)

 --------------------- função de inserção do valor ------------------------------

insert :: HTable -> Int -> IO (Bool)
insert table@(TH buckets n_elements len_tab onGrow) value = do
	tId <- myThreadId
	(slot,sizeTab,ptr) <- lockAcquire buckets len_tab value onGrow tId					-- adquire o lock específico da posição
	qtdInser <- readIORef n_elements
	if(elem value slot) then do
		lockRelease ptr sizeTab value slot
		return False
		else do
			if (threshold qtdInser sizeTab) then do
				nl <- let l = value:slot in l `seq` return l
				lockRelease ptr sizeTab value (nl)
				incHashInsert n_elements
				return True
				else do
					lockRelease ptr sizeTab value slot
					ok <- resize buckets sizeTab len_tab onGrow tId
					insert table value

-- -------------------------------------------------------------------------------

---- -------------------- função de consulta do valor ------------------------------
contains :: HTable -> Int -> IO (Bool)
contains table@(TH buckets n_elements len_tab onGrow) value = do
	tId <- myThreadId
	(slot,sizeTab,ptr) <- lockAcquire buckets len_tab value onGrow tId
	if (elem value slot) then do
		lockRelease ptr sizeTab value slot
		return True
		else do
			lockRelease ptr sizeTab value slot
			return False
------ -------------------------------------------------------------------------------

------ ------------------- função de remoção do valor --------------------------------
delete :: HTable -> Int -> IO (Bool)
delete table@(TH buckets n_elements len_tab onGrow) value = do
	tId <- myThreadId
	(slot,sizeTab,ptr) <- lockAcquire buckets len_tab value onGrow tId
	if (elem value slot) then do
		nl <- let l = filter (\x -> x /= value) slot in l `seq` return l
		lockRelease ptr sizeTab value nl
		decHashInsert n_elements
		return True
		else do
			lockRelease ptr sizeTab value slot
			return False

---- -------------------------------------------------------------------------------

---- ---------------- Funções auxiliares -------------------------------
atomCAS :: Eq a => IORef a -> a -> a -> IO Bool
atomCAS ptr old new =
   atomicModifyIORefCAS ptr (\ cur -> if cur == old
                                   then (new, True)
                                   else (cur, False))

incHashInsert :: IORef Int -> IO Bool
incHashInsert var = do
	old <- readIORef var
	new <- let n = old+1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else incHashInsert var

decHashInsert :: IORef Int -> IO Bool
decHashInsert var = do
	old <- readIORef var
	new <- let n = old-1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else decHashInsert var

lockAcquire :: Locks -> IORef Int -> Int -> IORef ThId -> ThreadId -> IO ([Int], Int, MVar [Int])
lockAcquire locks len_tab val flag tId = do
	x <- checkOnGrow flag tId 															-- verifica se a tabela esta em processo de cresimento
	sizeTab <- readIORef len_tab 														-- se não, le o valor do tamanho da tabela
	oldBuckets <- readIORef locks 														-- le o array de MVars
	sizeTab2 <- readIORef len_tab
	if(sizeTab == sizeTab2) then do
			lo <- tryTakeMVar $ oldBuckets ! (hashfun val sizeTab) 								-- tenta adquirir os locks correspondentes ao valor
			case lo of
				Just slot -> do
					sizeTab3 <- readIORef len_tab
					f <- readIORef flag
					if (f == Null || (sizeTab == sizeTab3)) then do
						return (slot,sizeTab,oldBuckets ! (hashfun val sizeTab))
						else do
							lockRelease (oldBuckets ! (hashfun val sizeTab)) sizeTab val slot 
							lockAcquire locks len_tab val flag tId							
				Nothing -> lockAcquire locks len_tab val flag tId
		else do
			lockAcquire locks len_tab val flag tId


checkOnGrow :: IORef ThId -> ThreadId -> IO Bool
checkOnGrow flag tId = do
	t <- readIORef flag
	if (t == Null) then return True else checkOnGrow flag tId

lockRelease :: MVar [Int] -> Int -> Int -> [Int] -> IO ()
lockRelease ptr sizeTab val slot = do
	putMVar ptr slot

------ -------------------------------------------------------------------------------------------

hashfun :: Int -> Int -> Int
hashfun val len_tab = mod val len_tab

threshold :: Int -> Int -> Bool
threshold n_elements sizeTab = fromIntegral n_elements * 0.75 <= fromIntegral sizeTab

copy_list :: [Int] -> Buckets -> Int -> IO Bool
copy_list [] array1 size_array = return True
copy_list (x:xs) array1 size_array = do
	list <- takeMVar (array1 ! hashfun x size_array)
	newList <- let nl = x:list in nl `seq` return nl
	putMVar (array1 ! hashfun x size_array) (newList)
	copy_list xs array1 size_array

resizeArray :: Buckets -> Buckets -> Int -> Int -> IO Buckets
resizeArray array1 array2 0 new_size = return array2
resizeArray array1 array2 old_size new_size = do
	lista <- takeMVar $ array1 ! (old_size-1)
	if (lista /= []) then do
			copy_list lista array2 new_size
			putMVar (array1 ! (old_size-1)) lista
			resizeArray array1 array2 (old_size-1) new_size
		else do
			putMVar (array1 ! (old_size-1)) lista
			resizeArray array1 array2 (old_size-1) new_size


---- ---------------Implementação do atomCAS ----------------------

resize :: IORef Buckets -> Int -> IORef Int -> IORef ThId -> ThreadId -> IO Bool
resize buckets old_size len_tab flag tId = do
	f <- readIORef flag
	if f == Null then do
		old_array <- readIORef buckets
		ok <- atomCAS flag f (ThId tId)
		if ok then do 
			sizeTab <- readIORef len_tab
			if(old_size == sizeTab) then do
				slots <- replicateM (old_size*2) (newMVar [])
				arrayHash <- let arrayHash' = listArray (0,(old_size*2)-1) slots in arrayHash' `seq` return arrayHash'
				new_array <- resizeArray old_array arrayHash old_size (old_size*2)
				writeIORef buckets new_array
				writeIORef len_tab (old_size*2)
				writeIORef flag Null
				return True
				else do
					writeIORef flag Null
					return False
			else return False
		else return False