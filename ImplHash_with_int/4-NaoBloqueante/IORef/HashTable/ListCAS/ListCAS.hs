{- Lista encadeada não bloqueante -}
module HashTable.ListCAS.ListCAS where
--module ListCAS.ListCAS where

import Data.IORef
--import Data.CAS
import Data.Word


data List = Node { val::Word, next :: IORef (List)} | DelNode {next :: IORef (List)} | Null | Guard {val::Word, next :: IORef (List)}
	deriving(Eq)

type  ListHandle = IORef List

--	deriving(Show)

atomCAS :: Eq a => IORef a -> a -> a -> IO Bool
atomCAS ptr old new =
	atomicModifyIORef  ptr (\cur -> if cur == old
								then (new,True)
								else (cur,False))

firstSlot :: IO List
firstSlot = do
	null <- newIORef Null
	return (Guard {val=0,next = null})

newEmptyList :: IO ListHandle
newEmptyList = newIORef Null >>= return

-- função que
newList :: IO ListHandle
newList = do
	null <- newIORef Null
	guard <- newIORef (Guard {val=0,next = null})
	return guard


-- função para adicionar um valor na lista encadeada
addToList :: ListHandle -> Word -> IO Bool
addToList list value = addList list list value
-- Modificando as implementações da função add para evitar um problema de condição de corrida ->
-- ==============================================================================================================================================================
addList :: IORef List -> IORef List -> Word -> IO Bool
addList listPtr listPtrBk value = do
	nodeVal <- readIORef listPtr
	case nodeVal of
		Node {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é um Nodo.
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then do
						addList nextPtr listPtrBk value
						else do
							newPtr <- newIORef Node {val=value, next=nextPtr}
							tst <- atomCAS listPtr nodeVal (Node {val=currVal,next=newPtr})
							if tst then return True else addList listPtrBk listPtrBk value

				DelNode {next=nextPtr2} -> do
					test <- atomCAS listPtr nodeVal (Node {val=currVal,next=nextPtr2})
					if test then addList listPtr listPtrBk value else addList listPtrBk listPtrBk value

				Node {val=nextVal,next=nextPtr2} -> do
					if (nextVal == value) then
						return False												-- se o valor ja existe então retorna Falso
						else do
							if (nextVal > value) then do 								-- se o proximo valor for maior, então insere aqui
								newPtr <- newIORef Node {val=value, next=nextPtr}
								tst <- atomCAS listPtr nodeVal (Node {val=currVal,next=newPtr})
								if tst then return True else addList listPtrBk listPtrBk value
								else addList nextPtr listPtrBk value 						-- caso contrario contiua buscando

				Null -> do
					newPtr <- newIORef Node {val=value, next=nextPtr}
					tst <- atomCAS listPtr nodeVal (Node {val=currVal,next=newPtr})
					if tst then return True else addList listPtrBk listPtrBk value

		Guard {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é uma Guarda (geralmente o caso inicial)
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do 								-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then do
						addList nextPtr listPtrBk value
						else do
							newPtr <- newIORef Node {val=value, next=nextPtr}
							tst <- atomCAS listPtr nodeVal (Guard {val=currVal,next=newPtr})
							if tst then return True else addList listPtrBk listPtrBk value

				DelNode {next=nextPtr2} -> do 											-- se o prox. for um nodo de Deleção, apaga ele fisicamente
					test <- atomCAS listPtr nodeVal (Guard {val=currVal,next=nextPtr2})
					if test then addList listPtr listPtrBk value else addList listPtrBk listPtrBk value

				Node {val=nextVal,next=nextPtr2} -> do 									-- se for um nodo tem de verificar o valor
					if (nextVal == value) then
						return False													-- se o valor ja existe então retorna Falso
						else do
							if (nextVal > value) then do 								-- se o proximo valor for maior, então insere aqui
								newPtr <- newIORef Node {val=value, next=nextPtr}
								tst <- atomCAS listPtr nodeVal (Guard {val=currVal,next=newPtr})
								if tst then return True else addList listPtrBk listPtrBk value
								else addList nextPtr listPtrBk value 						-- caso contrario contiua buscando

				Null -> do 																-- se chegou no final da lista, então pode ser inserido direto
					newPtr <- newIORef Node {val=value, next=nextPtr}
					tst <- atomCAS listPtr nodeVal (Guard {val=currVal,next=newPtr})
					if tst then return True else addList listPtrBk listPtrBk value

		DelNode {next=nextPtr} -> addList nextPtr listPtrBk value

		Null -> do
			newNext <- newIORef Null
			tst <- atomCAS listPtr nodeVal (Node {val=value, next=newNext})
			if tst then return True else addList listPtrBk listPtrBk value

-- ==============================================================================================================================================================



-- função que remove um valor da lista encadeada
delFromList :: ListHandle -> Word -> IO Bool
delFromList list value = delList3 list list value

delList3 :: IORef List -> IORef List -> Word -> IO Bool
delList3 listPtr listPtrBk value = do
	nodeVal <- readIORef listPtr
	case nodeVal of
		Node {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é um Nodo.
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then delList3 nextPtr listPtrBk value else return False

				DelNode {next=nextPtr2} -> do
					test <- atomCAS listPtr nodeVal (Node {val=currVal,next=nextPtr2})
					if test then delList3 listPtr listPtrBk value else delList3 listPtrBk listPtrBk value

				Node {val=nextVal,next=nextPtr2} -> do
					if (nextVal == value) then do
						tst <- atomCAS nextPtr nextNodeVal (DelNode {next=nextPtr2})
						if tst then return True else delList3 listPtrBk listPtrBk value
						else do
							if (nextVal > value) then return False else delList3 nextPtr listPtrBk value

				Null -> return False

		Guard {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é uma Guarda (geralmente o caso inicial)
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then delList3 nextPtr listPtrBk value else return False

				DelNode {next=nextPtr2} -> do 											-- se o prox. for um nodo de Deleção, apaga ele fisicamente
					test <- atomCAS listPtr nodeVal (Guard {val=currVal,next=nextPtr2})
					if test then delList3 listPtr listPtrBk value else delList3 listPtrBk listPtrBk value

				Node {val=nextVal,next=nextPtr2} -> do 									-- se for um nodo tem de verificar o valor
					if (nextVal == value) then do
						tst <- atomCAS nextPtr nextNodeVal (DelNode {next=nextPtr2})
						if tst then return True else delList3 listPtrBk listPtrBk value
						else do
							if (nextVal > value) then return False else delList3 nextPtr listPtrBk value 			-- caso contrario contiua buscando

				Null -> return False

		DelNode {next=nextPtr} -> delList3 nextPtr listPtrBk value

		Null -> return False

-- ==================================================================================================================================================================
findInList :: ListHandle -> Word -> IO Bool
findInList list value = findList3 list list value

findList3 :: IORef List -> IORef List -> Word -> IO Bool
findList3 listPtr listPtrBk value = do
	nodeVal <- readIORef listPtr
	case nodeVal of
		Node {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é um Nodo.
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then findList3 nextPtr listPtrBk value else return False

				DelNode {next=nextPtr2} -> do
					test <- atomCAS listPtr nodeVal (Node {val=currVal,next=nextPtr2})
					if test then findList3 listPtr listPtrBk value else findList3 listPtrBk listPtrBk value

				Node {val=nextVal,next=nextPtr2} -> do
					if (nextVal == value) then return True else findList3 nextPtr listPtrBk value

				Null -> return False

		Guard {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é uma Guarda (geralmente o caso inicial)
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then findList3 nextPtr listPtrBk value else return False

				DelNode {next=nextPtr2} -> do 											-- se o prox. for um nodo de Deleção, apaga ele fisicamente
					test <- atomCAS listPtr nodeVal (Guard {val=currVal,next=nextPtr2})
					if test then findList3 listPtr listPtrBk value else findList3 listPtrBk listPtrBk value
				Node {val=nextVal,next=nextPtr2} -> do 									-- se for um nodo tem de verificar o valor
					if (nextVal == value) then return True else findList3 nextPtr listPtrBk value

				Null -> return False

		DelNode {next=nextPtr} -> findList3 nextPtr listPtrBk value

		Null -> return False

-- ==================================================================================================================================================================

-- ================================================================================================================================================================
addGuardToList2 :: ListHandle -> Word -> IO ListHandle
addGuardToList2 list val = addGuard2 list list val

addGuard2 :: ListHandle -> ListHandle -> Word -> IO ListHandle
addGuard2 listPtr listPtrBk value = do
	prevNode <- readIORef listPtr
	case prevNode of
		Node {val=prevVal,next=nextPtr} -> do
			curr <- readIORef nextPtr
			case curr of
				Guard {val=currVal,next=currPtr} -> do
					if (currVal == value) then return nextPtr
						else do
							if (currVal > value) then do
								newGuard <- newIORef Guard {val=value,next=nextPtr}
								tst<-atomCAS listPtr prevNode (Node {val=prevVal,next=newGuard})
								if tst then
									return newGuard
									else addGuard2 listPtrBk listPtrBk value
								else addGuard2 nextPtr listPtrBk value

				DelNode {next=nextPtr2} -> do
							test <- atomCAS listPtr prevNode (Node {val=prevVal,next=nextPtr2})
							if test then addGuard2 listPtr listPtrBk value else addGuard2 listPtrBk listPtrBk value

				Node {val=currVal,next=currPtr} -> do
					if (currVal >= value) then do
						newGuard <- newIORef Guard {val=value,next=nextPtr}
						tst<-atomCAS listPtr prevNode (Node {val=prevVal,next=newGuard})
						if tst then
							return newGuard
							else addGuard2 listPtrBk listPtrBk value
						else addGuard2 nextPtr listPtrBk value
				Null -> do
					newGuard <- newIORef Guard {val=value,next=nextPtr}
					tst<-atomCAS listPtr prevNode (Node {val=prevVal,next=newGuard})
					if tst then
						return newGuard
						else addGuard2 listPtrBk listPtrBk value

		Guard {val=prevVal,next=nextPtr} -> do
			curr <- readIORef nextPtr
			case curr of
				Guard {val=currVal,next=currPtr} -> do
					if (currVal == value) then return nextPtr
						else do
							if(currVal > value) then do
								newGuard <- newIORef Guard {val=value,next=nextPtr}
								tst<-atomCAS listPtr prevNode (Guard {val=prevVal,next=newGuard})
								if tst then
									return newGuard
									else addGuard2 listPtrBk listPtrBk value
								else addGuard2 nextPtr listPtrBk value

				DelNode {next=nextPtr2} -> do 											-- se o prox. for um nodo de Deleção, apaga ele fisicamente
					test <- atomCAS listPtr prevNode (Guard {val=prevVal,next=nextPtr2})
					if test then addGuard2 listPtr listPtrBk value else addGuard2 listPtrBk listPtrBk value


				Node {val=currVal,next=currPtr} -> do
					if (currVal >= value) then do
						newGuard <- newIORef Guard {val=value,next=nextPtr}
						tst<-atomCAS listPtr prevNode (Guard {val=prevVal,next=newGuard})
						if tst then
							return newGuard
							else addGuard2 listPtrBk listPtrBk value
						else addGuard2 nextPtr listPtrBk value

				Null -> do
					newGuard <- newIORef Guard {val=value,next=nextPtr}
					tst<-atomCAS listPtr prevNode (Guard {val=prevVal,next=newGuard})
					if tst then
						return newGuard
						else addGuard2 listPtrBk listPtrBk value

		DelNode {next=nextPtr} -> addGuard2 nextPtr listPtrBk value

		Null -> do
			newNext <- newIORef Null
			tst <- atomCAS listPtr prevNode (Guard {val=value, next=newNext})
			if tst then return listPtr else addGuard2 listPtrBk listPtrBk value
-- ================================================================================================================================================================
