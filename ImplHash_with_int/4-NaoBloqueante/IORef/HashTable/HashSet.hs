module HashTable.HashSet where

import HashTable.ListCAS.ListCAS
--import ListCAS.ListCAS
import Data.Array
import Data.IORef
import Data.Bits
import Control.Monad
import Control.Concurrent
import Data.Word
import Foreign.C


data Slot = Lista {list::ListHandle} | Nil
	deriving(Eq)

type Buckets = Array Int (IORef Slot)

data HTable = TH
	{
	buckets	 :: IORef Buckets,  -- colocar a tabela em um IORef do Bem
	n_elemens :: IORef Int,
	len_tab  :: IORef Int
	}

newHash :: Int -> IO (HTable)
newHash initSize = do
	x <- replicateM initSize (newIORef Nil)
	let arrayHash = listArray (0,initSize-1) x
	let first = arrayHash ! 0
	headList <- newList
	writeIORef first Lista {list=headList}
	buckets <- newIORef arrayHash
	n_elemens <- newIORef 0
	len_tab <- newIORef initSize
	return (TH buckets n_elemens len_tab)


insert :: HTable -> Int -> IO (Bool)
insert table@(TH buckets n_elemens len_tab) value = do
	tHash <- readIORef len_tab
	slots <- readIORef buckets
	nInsert <- readIORef n_elemens
	if (threshold nInsert tHash) then do
		v <- readIORef $ slots ! hashfun value tHash
		case v of
			Lista {list=listaha} -> do
				inverseVal <- reverseBits value
				tst <- addToList listaha inverseVal
				if tst then do
					incHashInsert n_elemens
					return tst
					else return tst
			Nil -> do
				tst <- initializeGuard slots tHash $ hashfun value tHash
				inverseVal <- reverseBits value
				tst2 <- addToList tst inverseVal
				if tst2 then do
					incHashInsert n_elemens
					return tst2
					else return tst2
		else do
			x <- duplicaTabela slots tHash
			tHash2 <- readIORef len_tab
			if (tHash2 == tHash) then do
				tst <- atomCAS buckets slots x
				if tst then do
					tst2 <- atomCAS len_tab tHash (tHash*2)
					insert table value
					else do
						insert table value
				else do
					insert table value


delete :: HTable -> Int -> IO (Bool)
delete table@(TH buckets n_elemens len_tab) value = do
	tHash <- readIORef len_tab
	slots <- readIORef buckets
	ptrList <- readIORef $ slots ! hashfun value tHash
	case ptrList of
		Lista {list=listaha} -> do
			inverseVal <- reverseBits value
			tst <- delFromList listaha inverseVal
			if tst then do
				decHashInsert n_elemens
				return tst
				else
					return tst
		Nil -> do
			tst <- initializeGuard slots tHash $ hashfun value tHash
			inverseVal <- reverseBits value
			tst2 <- delFromList tst inverseVal
			if tst2 then do
				decHashInsert n_elemens
				return tst2
				else return tst2

contains :: HTable -> Int -> IO (Bool)
contains table@(TH buckets n_elemens len_tab) value = do
	tHash <- readIORef len_tab
	slots <- readIORef buckets
	prtList <- readIORef $ slots ! hashfun value tHash
	case prtList of
		Lista {list=listaha} -> do
			inverseVal <- reverseBits value
			tst <- findInList listaha inverseVal
			return tst
		Nil -> do
			tst <- initializeGuard slots tHash $ hashfun value tHash
			inverseVal <- reverseBits value
			tst2 <- findInList tst inverseVal
			return tst2

-- FUNÇÔES AUXILIARES --------------------------------------------------------------
hashfun :: Int -> Int -> Int
hashfun val len_tab = mod val len_tab

foreign import ccall safe reverseBits :: Int -> IO Word

incHashInsert :: IORef Int -> IO Bool
incHashInsert var = do
	old <- readIORef var
	new <- let n = old+1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else incHashInsert var

decHashInsert :: IORef Int -> IO Bool
decHashInsert var = do
	old <- readIORef var
	new <- let n = old-1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else decHashInsert var

initializeGuard :: Buckets -> Int -> Int -> IO ListHandle
initializeGuard slots sizeTab guardVal = do
	x <- readIORef $ slots ! searchParent sizeTab guardVal
	case x of
		Lista {list = lista} -> do
			inverseVal <- reverseBits guardVal
			a <- addGuardToList2 lista	inverseVal
			--atomicWriteIORef (slots ! guardVal) (Lista {list=a})
			writeIORef (slots ! guardVal) (Lista {list=a})
			return a
		Nil -> do
			newGuard <- initializeGuard slots sizeTab $ searchParent sizeTab guardVal
			inverseVal <- reverseBits guardVal
			a <- addGuardToList2 newGuard inverseVal
			--atomicWriteIORef (slots ! guardVal) (Lista {list=a})
			writeIORef (slots ! guardVal) (Lista {list=a})
			return a


searchParent :: Int -> Int -> Int
searchParent sizeTab endSlot | newPos > endSlot = searchParent newPos endSlot
						  	| otherwise = endSlot - newPos
	where newPos = shiftR sizeTab 1

duplicaTabela :: Buckets -> Int -> IO Buckets
duplicaTabela slots sizeTab = do
	let oldList = elems slots
	x <- replicateM (sizeTab*2) (newIORef Nil)
	let newArrayHash = listArray (0,(sizeTab*2)-1) $ oldList++x
	return newArrayHash

threshold :: Int -> Int -> Bool
threshold n_elemens sizeTab = fromIntegral n_elemens * 0.75 <= fromIntegral sizeTab
