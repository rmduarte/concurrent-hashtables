module HashTable.HashSet where

import HashTable.ListCAS.ListCAS
--import ListCAS.ListCAS
import Data.Array
import Data.Bits
import Control.Monad
import Control.Concurrent.STM
import GHC.Conc.Sync
import Data.Word
import Foreign.C


data Slot = Lista {list::ListHandle} | Nil
	deriving(Eq)

type Buckets = Array Int (TVar Slot)

data HTable = TH 
	{
	buckets	 :: TVar Buckets,  -- colocar a tabela em um TVar do Bem 
	n_elemens :: TVar Int,
	len_tab  :: TVar Int
	}

newHash :: Int -> STM (HTable)
newHash initSize = do
	x <- replicateM initSize (newTVar Nil)
	let arrayHash = listArray (0,initSize-1) x
	let first = arrayHash ! 0
	headList <- newList
	writeTVar first Lista {list=headList}
	buckets <- newTVar arrayHash
	n_elemens <- newTVar 0
	len_tab <- newTVar initSize
	return (TH buckets n_elemens len_tab)


insert :: HTable -> Int -> STM (Bool)
insert table@(TH buckets n_elemens len_tab) value = do
	tHash <- readTVar len_tab
	slots <- readTVar buckets
	nInsert <- readTVar n_elemens
	if (threshold nInsert tHash) then do
		v <- readTVar $ slots ! hashfun value tHash
		case v of
			Lista {list=listaha} -> do
				inverseVal <- reverseBits' value
				tst <- addToList listaha inverseVal
				if tst then do
					incHashInsert n_elemens
					return tst
					else return tst
			Nil -> do
				tst <- initializeGuard slots tHash $ hashfun value tHash
				inverseVal <- reverseBits' value
				tst2 <- addToList tst inverseVal
				if tst2 then do
					incHashInsert n_elemens
					return tst2
					else return tst2
		else do
			x <- duplicaTabela slots tHash
			writeTVar buckets x
			writeTVar len_tab (tHash*2)
			insert table value


delete :: HTable -> Int -> STM (Bool)
delete table@(TH buckets n_elemens len_tab) value = do
	tHash <- readTVar len_tab
	slots <- readTVar buckets
	ptrList <- readTVar $ slots ! hashfun value tHash
	case ptrList of
		Lista {list=listaha} -> do
			inverseVal <- reverseBits' value
			tst <- delFromList listaha inverseVal
			if tst then do
				decHashInsert n_elemens
				return tst
				else
					return tst
		Nil -> do
			tst <- initializeGuard slots tHash $ hashfun value tHash
			inverseVal <- reverseBits' value
			tst2 <- delFromList tst inverseVal
			if tst2 then do
				decHashInsert n_elemens
				return tst2
				else return tst2

contains :: HTable -> Int -> STM (Bool)
contains table@(TH buckets n_elemens len_tab) value = do
	tHash <- readTVar len_tab
	slots <- readTVar buckets
	prtList <- readTVar $ slots ! hashfun value tHash
	case prtList of
		Lista {list=listaha} -> do
			inverseVal <- reverseBits' value
			tst <- findInList listaha inverseVal
			return tst
		Nil -> do
			tst <- initializeGuard slots tHash $ hashfun value tHash
			inverseVal <- reverseBits' value
			tst2 <- findInList tst inverseVal
			return tst2

-- FUNÇÔES AUXILIARES --------------------------------------------------------------
hashfun :: Int -> Int -> Int
hashfun val len_tab = mod val len_tab 

foreign import ccall safe reverseBits :: Int -> IO Word

reverseBits' :: Int -> STM Word
reverseBits' val = unsafeIOToSTM (reverseBits val) >>= (\rev -> return rev)

incHashInsert :: TVar Int -> STM ()
incHashInsert var = do
	old <- readTVar var
	writeTVar var (old+1)

decHashInsert :: TVar Int -> STM ()
decHashInsert var = do
	old <- readTVar var
	writeTVar var (old+1)

initializeGuard :: Buckets -> Int -> Int -> STM ListHandle
initializeGuard slots sizeTab guardVal = do
	x <- readTVar $ slots ! searchParent sizeTab guardVal
	case x of
		Lista {list = lista} -> do
			inverseVal <- reverseBits' guardVal
			a <- addGuardToList2 lista	inverseVal
			writeTVar (slots ! guardVal) (Lista {list=a})
			return a
		Nil -> do
			newGuard <- initializeGuard slots sizeTab $ searchParent sizeTab guardVal
			inverseVal <- reverseBits' guardVal
			a <- addGuardToList2 newGuard inverseVal
			writeTVar (slots ! guardVal) (Lista {list=a})
			return a


searchParent :: Int -> Int -> Int
searchParent sizeTab endSlot | newPos > endSlot = searchParent newPos endSlot
						  	| otherwise = endSlot - newPos
	where newPos = shiftR sizeTab 1

duplicaTabela :: Buckets -> Int -> STM Buckets
duplicaTabela slots sizeTab = do
	let oldList = elems slots
	x <- replicateM (sizeTab*2) (newTVar Nil)
	let newArrayHash = listArray (0,(sizeTab*2)-1) $ oldList++x
	return newArrayHash

threshold :: Int -> Int -> Bool
threshold n_elemens sizeTab = fromIntegral n_elemens * 0.75 <= fromIntegral sizeTab
