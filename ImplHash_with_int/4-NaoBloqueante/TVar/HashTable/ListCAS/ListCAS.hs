{- Lista encadeada não bloqueante -}
module HashTable.ListCAS.ListCAS where
--module ListCAS.ListCAS where

import Control.Concurrent.STM
import Data.Word


data List = Node { val::Word, next :: TVar (List)} | Null | Guard {val::Word, next :: TVar (List)}
	deriving(Eq)

type  ListHandle = TVar List

--	deriving(Show)

firstSlot :: STM List
firstSlot = do
	null <- newTVar Null
	return (Guard {val=0,next = null})

newEmptyList :: STM ListHandle
newEmptyList = newTVar Null >>= return

-- função que
newList :: STM ListHandle
newList = do 
	null <- newTVar Null
	guard <- newTVar (Guard {val=0,next = null})
	return guard


-- função para adicionar um valor na lista encadeada
addToList :: ListHandle -> Word -> STM Bool
addToList list value = addList list list value
-- Modificando as implementações da função add para evitar um problema de condição de corrida ->
-- ==============================================================================================================================================================
addList :: TVar List -> TVar List -> Word -> STM Bool
addList listPtr listPtrBk value = do
	nodeVal <- readTVar listPtr
	case nodeVal of
		Node {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é um Nodo.
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then do
						addList nextPtr listPtrBk value
						else do
							newPtr <- newTVar Node {val=value, next=nextPtr}
							writeTVar listPtr (Node {val=currVal,next=newPtr})
							return True
				
				Node {val=nextVal,next=nextPtr2} -> do
					if (nextVal == value) then 
						return False												-- se o valor ja existe então retorna Falso
						else do
							if (nextVal > value) then do 								-- se o proximo valor for maior, então insere aqui
								newPtr <- newTVar Node {val=value, next=nextPtr}
								writeTVar listPtr (Node {val=currVal,next=newPtr})
								return True
								else addList nextPtr listPtrBk value 						-- caso contrario contiua buscando

				Null -> do
					newPtr <- newTVar Node {val=value, next=nextPtr}
					writeTVar listPtr (Node {val=currVal,next=newPtr})
					return True

		Guard {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é uma Guarda (geralmente o caso inicial)
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do 								-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then do
						addList nextPtr listPtrBk value
						else do
							newPtr <- newTVar Node {val=value, next=nextPtr}
							writeTVar listPtr (Guard {val=currVal,next=newPtr})
							return True

				Node {val=nextVal,next=nextPtr2} -> do 									-- se for um nodo tem de verificar o valor
					if (nextVal == value) then 
						return False													-- se o valor ja existe então retorna Falso
						else do
							if (nextVal > value) then do 								-- se o proximo valor for maior, então insere aqui
								newPtr <- newTVar Node {val=value, next=nextPtr}
								writeTVar listPtr (Guard {val=currVal,next=newPtr})
								return True
								else addList nextPtr listPtrBk value 						-- caso contrario contiua buscando

				Null -> do 																-- se chegou no final da lista, então pode ser inserido direto
					newPtr <- newTVar Node {val=value, next=nextPtr}
					writeTVar listPtr (Guard {val=currVal,next=newPtr})
					return True

		Null -> do
			newNext <- newTVar Null
			writeTVar listPtr (Node {val=value, next=newNext})
			return True

-- ==============================================================================================================================================================



-- função que remove um valor da lista encadeada
delFromList :: ListHandle -> Word -> STM Bool
delFromList list value = delList3 list list value

delList3 :: TVar List -> TVar List -> Word -> STM Bool
delList3 listPtr listPtrBk value = do
	nodeVal <- readTVar listPtr
	case nodeVal of
		Node {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é um Nodo.
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then delList3 nextPtr listPtrBk value else return False
				
				Node {val=nextVal,next=nextPtr2} -> do
					if (nextVal == value) then do
						writeTVar listPtr (Node {val=currVal,next=nextPtr2})
						return True
						else do
							if (nextVal > value) then return False else delList3 nextPtr listPtrBk value

				Null -> return False

		Guard {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é uma Guarda (geralmente o caso inicial)
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then delList3 nextPtr listPtrBk value else return False

				Node {val=nextVal,next=nextPtr2} -> do 									-- se for um nodo tem de verificar o valor
					if (nextVal == value) then do
						writeTVar listPtr (Guard {val=currVal,next=nextPtr2})
						return True
						else do
							if (nextVal > value) then return False else delList3 nextPtr listPtrBk value 			-- caso contrario contiua buscando

				Null -> return False

		Null -> return False

-- ==================================================================================================================================================================
findInList :: ListHandle -> Word -> STM Bool
findInList list value = findList3 list list value

findList3 :: TVar List -> TVar List -> Word -> STM Bool
findList3 listPtr listPtrBk value = do
	nodeVal <- readTVar listPtr
	case nodeVal of
		Node {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é um Nodo.
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then findList3 nextPtr listPtrBk value else return False
				
				Node {val=nextVal,next=nextPtr2} -> do
					if (nextVal == value) then return True else findList3 nextPtr listPtrBk value

				Null -> return False

		Guard {val=currVal,next=nextPtr} -> do 											-- Caso em que o nodo é uma Guarda (geralmente o caso inicial)
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {val=nextVal,next=nextPtr2} -> do									-- se o proximo nodo for uma guarda é porque chegou no final da seção da lista
					if (nextVal <= value) then findList3 nextPtr listPtrBk value else return False

				Node {val=nextVal,next=nextPtr2} -> do 									-- se for um nodo tem de verificar o valor
					if (nextVal == value) then return True else findList3 nextPtr listPtrBk value

				Null -> return False

		Null -> return False

-- ==================================================================================================================================================================

-- ================================================================================================================================================================
addGuardToList2 :: ListHandle -> Word -> STM ListHandle
addGuardToList2 list val = addGuard2 list list val

addGuard2 :: ListHandle -> ListHandle -> Word -> STM ListHandle
addGuard2 listPtr listPtrBk value = do
	prevNode <- readTVar listPtr
	case prevNode of
		Node {val=prevVal,next=nextPtr} -> do
			curr <- readTVar nextPtr
			case curr of
				Guard {val=currVal,next=currPtr} -> do
					if (currVal == value) then return nextPtr
						else do
							if (currVal > value) then do
								newGuard <- newTVar Guard {val=value,next=nextPtr}
								writeTVar listPtr (Node {val=prevVal,next=newGuard})
								return newGuard
								else addGuard2 nextPtr listPtrBk value

				Node {val=currVal,next=currPtr} -> do
					if (currVal >= value) then do
						newGuard <- newTVar Guard {val=value,next=nextPtr}
						writeTVar listPtr (Node {val=prevVal,next=newGuard})
						return newGuard
						else addGuard2 nextPtr listPtrBk value

				Null -> do
					newGuard <- newTVar Guard {val=value,next=nextPtr}
					writeTVar listPtr (Node {val=prevVal,next=newGuard})
					return newGuard

		Guard {val=prevVal,next=nextPtr} -> do
			curr <- readTVar nextPtr
			case curr of
				Guard {val=currVal,next=currPtr} -> do
					if (currVal == value) then return nextPtr
						else do
							if(currVal > value) then do
								newGuard <- newTVar Guard {val=value,next=nextPtr}
								writeTVar listPtr (Guard {val=prevVal,next=newGuard})
								return newGuard
								else addGuard2 nextPtr listPtrBk value

				Node {val=currVal,next=currPtr} -> do
					if (currVal >= value) then do
						newGuard <- newTVar Guard {val=value,next=nextPtr}
						writeTVar listPtr (Guard {val=prevVal,next=newGuard})
						return newGuard
						else addGuard2 nextPtr listPtrBk value
				
				Null -> do
					newGuard <- newTVar Guard {val=value,next=nextPtr}
					writeTVar listPtr (Guard {val=prevVal,next=newGuard})
					return newGuard

		Null -> do
			newNext <- newTVar Null
			writeTVar listPtr (Guard {val=value, next=newNext})
			return listPtr
-- ================================================================================================================================================================