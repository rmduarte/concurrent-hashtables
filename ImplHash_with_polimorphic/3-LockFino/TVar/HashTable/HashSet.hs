module HashTable.HashSet where

import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.STM

import GHC.Conc.Sync

type Buckets k v = Array Int (TVar [(k,v)])

data HTable k v = TH
	{
	buckets	 :: TVar (Buckets k v),  -- colocar a tabela em um IORef do Bem
	n_elemens :: TVar Int,
	len_tab	 :: TVar Int,
	fHash	 :: (k -> Int)
	}
	
fhash :: Int -> Int
fhash a = a

newHash :: Int -> (k -> Int) -> STM (HTable k v)
newHash size fHash = do
	slots <- replicateM size (newTVar [])
	arrayHash <- let arrayHash' = listArray (0,size-1) slots in arrayHash' `seq` return arrayHash'
	buckets <- newTVar arrayHash
	n_elemens <- newTVar 0
	len_tab <- newTVar size
	return (TH buckets n_elemens len_tab fHash)


-- --------------------- função de inserção do valor ------------------------------
insert :: Eq k => Eq v => HTable k v -> k -> v -> STM Bool
insert table@(TH buckets n_elemens len_tab fHash) key value = do
	size <- readTVar len_tab
	nInsert <- readTVar n_elemens
	let pos = hashfun (fHash key) size
	x <- readTVar buckets
	slot <- readTVar $ x ! pos
	case lookup key slot of
		Just a -> return False
		Nothing -> if(threshold nInsert size) then do
						nl <- let l = (key,value):slot in l `seq` return l
						writeTVar (x ! pos) nl
						writeTVar n_elemens (nInsert+1)
						return True
						else do
							new_hash <- resize x size fHash
							let pos_hash = hashfun (fHash key) (size * 2)
							slot <- readTVar $ new_hash ! pos_hash
							nl <- let l = (key,value):slot in l `seq` return l
							writeTVar (new_hash ! pos_hash) (nl)
							writeTVar buckets new_hash
							writeTVar len_tab (size * 2)
							writeTVar n_elemens (nInsert + 1)
							return True
---- -------------------------------------------------------------------------------

---- -------------------- função de consulta do valor ------------------------------

contains :: Eq k => Eq v => HTable k v -> k -> STM (Maybe v)
contains table@(TH buckets n_elemens len_tab fHash) key = do
	size <- readTVar len_tab													-- le o tamanho da tabela
	slot <- (\x -> readTVar $ x ! hashfun (fHash key) size) =<< readTVar buckets-- busca o slot correspondente a chave
	return $ lookup key slot
---- -------------------------------------------------------------------------------

---- ------------------- função de remoção do valor --------------------------------
delete :: Eq k => Eq v => HTable k v -> k -> STM Bool
delete table@(TH buckets n_elemens len_tab fHash) key = do
	size <- readTVar len_tab
	x <- readTVar buckets
	let pos = hashfun (fHash key) size
	slot <- readTVar $ x ! pos
	case lookup key slot of
		Just a -> do
			writeTVar (x ! pos) $ filter (\x -> key /= fst x) slot
			readTVar n_elemens >>= \x -> writeTVar n_elemens $ x - 1
			return True
		Nothing -> return False
---- -------------------------------------------------------------------------------

-- ---- ---------------- Funções auxiliares ----------------------------------------
hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab

threshold :: Int -> Int -> Bool
threshold n_elemens tamTab = fromIntegral n_elemens * 0.75 <= fromIntegral tamTab

copy_list :: Eq k => Eq v => [(k,v)] -> Buckets k v -> Int -> (k -> Int) ->  STM Bool
copy_list [] array1 tam_array fhash = return True
copy_list ((key,value):xs) array1 tam_array fhash = do
	let array_pos = hashfun (fhash key) tam_array
	list <- readTVar (array1 ! array_pos)
	newList <- let nl = (key,value):list in nl `seq` return nl
	writeTVar (array1 ! array_pos) (newList)
	copy_list xs array1 tam_array fhash

resizeArray :: Eq k => Eq v => Buckets k v -> Buckets k v -> Int -> Int -> (k -> Int) -> STM (Buckets k v)
resizeArray array1 array2 0 new_tam fhash = return array2
resizeArray array1 array2 old_tam new_tam fhash = do
	lista <- readTVar $ array1 ! (old_tam-1)
	if (lista /= []) then do
		copy_list lista array2 new_tam fhash
		resizeArray array1 array2 (old_tam-1) new_tam fhash
		else resizeArray array1 array2 (old_tam-1) new_tam fhash

resize :: Eq k => Eq v => Buckets k v -> Int -> (k -> Int) -> STM (Buckets k v)
resize old_array old_tam fhash = do
	buckets <- replicateM (old_tam*2) (newTVar [])
	let arrayHash = listArray (0,(old_tam*2)-1) buckets
	new_array <- resizeArray old_array arrayHash old_tam (old_tam*2) fhash
	return new_array
