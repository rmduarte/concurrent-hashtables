module HashTable.HashSet where

import HashTable.ListCAS.ListCAS
--import ListCAS.ListCAS
import Data.Array
import Data.Bits
import Control.Monad
import Control.Concurrent.STM
import GHC.Conc.Sync
import Data.Word
import Foreign.C


data Slot v = Head {list::ListHandle v} | Nil
	deriving(Eq)

type Buckets v = Array Int (TVar (Slot v))

data HTable k v = TH
	{
	buckets	 :: TVar (Buckets v),
	n_elemens :: TVar Int,
	len_tab  :: TVar Int,
	fHash	 :: k -> Int
	}

fhash :: Int -> Int
fhash a = a

newHash :: Int -> (k -> Int) ->STM (HTable k v)
newHash size fHash = do
	slots <- replicateM size $ newTVar Nil
	let arrayHash = listArray (0,size-1) slots
	newList >>= \x -> writeTVar (arrayHash ! 0) Head {list=x}
	buckets <- newTVar arrayHash
	n_elemens <- newTVar 0
	len_tab <- newTVar size
	return (TH buckets n_elemens len_tab fHash)

insert :: Eq k => Eq v => HTable k v -> k -> v -> STM Bool
insert table@(TH buckets n_elemens len_tab fHash) key value = do
	size <- readTVar len_tab
	slots <- readTVar buckets
	nInsert <- readTVar n_elemens
	reverseKey <- reverseBits' $ fHash key
	if (threshold nInsert size) then do
		guard <- readTVar $ slots ! hashfun (fHash key) size
		case guard of
			Head {list=listGuard} -> do
				tst <- addToList listGuard reverseKey value
				if tst then do
					incHashInsert n_elemens
					return tst
					else return tst

			Nil -> do
				guard <- initializeGuard slots size $ hashfun (fHash key) size
				tst <- addToList guard reverseKey value
				if tst then do
					incHashInsert n_elemens
					return tst
					else return tst

		else do
			newSlots <- doubleTable slots size
			writeTVar buckets newSlots
			writeTVar len_tab (size*2)
			insert table key value


-- function to delete a element from list
delete :: Eq k => Eq v => HTable k v -> k -> STM Bool
delete table@(TH buckets n_elemens len_tab fHash) key = do
	size <- readTVar len_tab
	slots <- readTVar buckets
	reverseKey <- reverseBits' $ fHash key
	ptrList <- readTVar $ slots ! hashfun (fHash key) size
	case ptrList of
		Head {list=listGuard} -> do
			tst <- delFromList listGuard reverseKey
			if tst then do
				decHashInsert n_elemens
				return tst
				else
					return tst
		Nil -> do
	 		guard <- initializeGuard slots size $ hashfun (fHash key) size
		 	tst <- delFromList guard reverseKey
		 	if tst then do
		 		decHashInsert n_elemens
		 		return tst
		 		else return tst



-- function to search a element in list
contains :: Eq k => Eq v => HTable k v -> k -> STM (Maybe v)
contains table@(TH buckets n_elemens len_tab fHash) key = do
	size <- readTVar len_tab
	slots <- readTVar buckets
	reverseKey <- reverseBits' $ fHash key
 	prtList <- readTVar $ slots ! hashfun (fHash key) size
 	case prtList of
 		Head {list=listGuard} -> do
 			tst <- findInList listGuard reverseKey
 			return tst
 	 	Nil -> do
 	 		guard <- initializeGuard slots size $ hashfun (fHash key) size
 	 		tst <- findInList guard reverseKey
 	 		return tst

-- Auxiliar Functions --------------------------------------------------------------
hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab

foreign import ccall safe reverseBits :: Int -> IO Word

reverseBits' :: Int -> STM Word
reverseBits' val = unsafeIOToSTM (reverseBits val) >>= (\rev -> return rev)

incHashInsert :: TVar Int -> STM ()
incHashInsert var = do
	old <- readTVar var
	writeTVar var (old+1)

decHashInsert :: TVar Int -> STM ()
decHashInsert var = do
	old <- readTVar var
	writeTVar var (old+1)

initializeGuard :: Eq v => Buckets v -> Int -> Int -> STM (ListHandle v)
initializeGuard slots size key = do
	x <- readTVar $ slots ! searchParent size key
	reverseKey <- reverseBits' key
	case x of
		Head {list=listGuard} -> do
			guard <- addGuardToList listGuard reverseKey
			writeTVar (slots ! key) Head {list=guard}
			return guard

		Nil -> do
			newGuard <- initializeGuard slots size $ searchParent size key
			guard <- addGuardToList newGuard reverseKey
			writeTVar (slots ! key) (Head {list=guard})
			return guard


searchParent :: Int -> Int -> Int
searchParent tamTab endSlot | newPos > endSlot = searchParent newPos endSlot
						  	| otherwise = endSlot - newPos
 	where newPos = shiftR tamTab 1

doubleTable :: Buckets v -> Int -> STM (Buckets v)
doubleTable slots size = do
	let oldList = elems slots
	x <- replicateM (size*2) (newTVar Nil)
	let newArrayHash = listArray (0,(size*2)-1) $ oldList++x
	return newArrayHash

threshold :: Int -> Int -> Bool
threshold n_elemens tamTab = fromIntegral n_elemens * 0.75 <= fromIntegral tamTab
