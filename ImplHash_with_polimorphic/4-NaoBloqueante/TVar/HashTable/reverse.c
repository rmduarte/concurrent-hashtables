#include <stdio.h>

volatile long int reverseBits(long int val){
	//printf("o Tamanho do int é %lu\n",sizeof(long int));
	long int i,a,b;
	long int result = 0;
	b = 0;
	a = val;
	for (i=0;i<64;i++){
		result = (a & 0x1) | b;
		//printf("Os valores %lu são: %lu e %lu e %lu\n",i,val,a,result );
		a = a >> 1;
		b = result << 1;
	}
	//printf("Valor passado é: %lu\n",result );
	return result;
}
