{- Lista encadeada não bloqueante -}
module HashTable.ListCAS.ListCAS where
--module ListCAS.ListCAS where

import Control.Concurrent.STM
import Data.Word


data List v = Node {nodeVal::(Word,v), next :: TVar (List v)}
		  | Null
		  | Guard {guardVal::Word, next :: TVar (List v)}
	deriving(Eq)

type  ListHandle v = TVar (List v)

-- function to start the new list
newList :: STM (ListHandle v)
newList = do
	newTVar Null >>= \x -> newTVar (Guard {guardVal=0,next = x}) >>= return


-- function to add a elemnt in the list
addToList :: Eq v => ListHandle v -> Word -> v -> STM Bool
addToList list key value = addList list list key value

-- -- ==============================================================================================================================================================
addList :: Eq v => ListHandle v -> ListHandle v -> Word -> v -> STM Bool
addList listPtr listPtrBk key value = do
	currNodeVal <- readTVar listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey, next=nextPtr2} -> do
					if (nextKey <= key) then do
						addList nextPtr listPtrBk key value
						else do
							newNode <- newTVar Node {nodeVal=(key,value),next=nextPtr}
							writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=newNode})
							return True
				Node {nodeVal=(nextKey,nextVal), next=nextPtr2} -> do
					if ((nextKey,nextVal) == (key,value)) then
						return False
						else do
							if (nextKey > key) then do
								newNode <- newTVar Node {nodeVal=(key,value),next=nextPtr}
								writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=newNode})
								return True
								else addList nextPtr listPtrBk key value

				Null -> do
					newNode <- newTVar Node {nodeVal=(key,value),next=nextPtr}
					writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=newNode})
					return True

		Guard {guardVal=currKey,next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then do
						addList nextPtr listPtrBk key value
						else do
							newNode <- newTVar Node {nodeVal=(key,value),next=nextPtr}
							writeTVar listPtr (Guard {guardVal=currKey,next=newNode})
							return True
				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if ((nextKey,nextVal) == (key,value)) then
						return False
						else do
							if (nextKey > key) then do
								newNode <- newTVar Node {nodeVal=(key,value),next=nextPtr}
								writeTVar listPtr (Guard {guardVal=currKey,next=newNode})
								return True
								else addList nextPtr listPtrBk key value

				Null -> do
					newNode <- newTVar Node {nodeVal=(key,value), next=nextPtr}
					writeTVar listPtr (Guard {guardVal=currKey,next=newNode})
					return True

		Null -> do
			newNext <- newTVar Null
			writeTVar listPtr (Node {nodeVal=(key,value),next=newNext})
			return True

-- ==============================================================================================================================================================
-- function to delete a element from list
delFromList :: Eq v => ListHandle v -> Word -> STM Bool
delFromList list key = delList list list key

delList :: Eq v => ListHandle v -> ListHandle v -> Word -> STM Bool
delList listPtr listPtrBk key = do
	currNodeVal <- readTVar listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then delList nextPtr listPtrBk key else return False

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then do
						writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=nextPtr2})
						return True
						else do
							if (nextKey > key) then return False else delList nextPtr listPtrBk key

				Null -> return False

		Guard {guardVal=currKey,next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then delList nextPtr listPtrBk key else return False

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then do
						writeTVar listPtr (Guard {guardVal=currKey,next=nextPtr2})
						return True
						else do
							if (nextKey > key) then return False else delList nextPtr listPtrBk key

				Null -> return False

		Null -> return False
-- ==================================================================================================================================================================
-- function to find a element from list
findInList :: Eq v => ListHandle v -> Word -> STM (Maybe v)
findInList list key = findList list list key

findList :: Eq v => ListHandle v -> ListHandle v -> Word -> STM (Maybe v)
findList listPtr listPtrBk key = do
	currNodeVal <- readTVar listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if(nextKey <= key) then findList nextPtr listPtrBk key else return Nothing

				Node {nodeVal=(nextKey,nextVal), next=nextPtr2} -> do
					if(nextKey == key) then return $ Just nextVal else findList nextPtr listPtrBk key

				Null -> return Nothing

		Guard {guardVal=currKey, next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey, next=nextPtr2} -> do
					if(nextKey <= key) then findList nextPtr listPtrBk key else return Nothing

				Node {nodeVal=(nextKey,nextVal), next=nextPtr2} -> do
					if (nextKey == key) then return $ Just nextVal else findList nextPtr listPtrBk key

				Null -> return Nothing

		Null -> return Nothing
-- ==================================================================================================================================================================
addGuardToList :: Eq v => ListHandle v -> Word -> STM (ListHandle v)
addGuardToList list key = addGuard list list key

addGuard :: Eq v => ListHandle v -> ListHandle v -> Word -> STM (ListHandle v)
addGuard listPtr listPtrBk key = do
	currNodeVal <- readTVar listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readTVar nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if(nextKey==key) then return nextPtr
						else do
							if (nextKey > key) then do
								newGuard <- newTVar Guard {guardVal=key,next=nextPtr}
								writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=newGuard})
								return newGuard
								else addGuard nextPtr listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey >= key) then do
						newGuard <- newTVar Guard {guardVal=key,next=nextPtr}
						writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=newGuard})
						return newGuard
						else addGuard nextPtr listPtrBk key

				Null -> do
					newGuard <- newTVar Guard {guardVal=key,next=nextPtr}
					writeTVar listPtr (Node {nodeVal=(currKey,currVal),next=newGuard})
					return newGuard

		Guard {guardVal=currKey,next=nextPtr} -> do
			currNodeVal <- readTVar nextPtr
			case currNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey == key) then return nextPtr
						else do
							if(nextKey > key) then do
								newGuard <- newTVar Guard {guardVal=key,next=nextPtr}
								writeTVar listPtr (Guard {guardVal=currKey,next=newGuard})
								return newGuard
								else addGuard nextPtr listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if(nextKey >= key) then do
						newGuard <- newTVar Guard {guardVal=key,next=nextPtr}
						writeTVar listPtr (Guard {guardVal=currKey,next=newGuard})
						return newGuard
						else addGuard nextPtr listPtrBk key

				Null -> do
					newGuard <- newTVar Guard {guardVal=key,next=nextPtr}
					writeTVar listPtr (Guard {guardVal=currKey,next=newGuard})
					return newGuard

		Null -> do
			newNext <- newTVar Null
			writeTVar listPtr (Guard {guardVal=key, next=newNext})
			return listPtr
-- -- ================================================================================================================================================================
