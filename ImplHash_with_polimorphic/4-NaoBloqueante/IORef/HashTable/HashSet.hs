module HashTable.HashSet where

import HashTable.ListCAS.ListCAS
--import ListCAS.ListCAS
import Data.Array
import Data.IORef
import Data.Bits
import Control.Monad
import Control.Concurrent
import Data.Word
import Foreign.C


data Slot v = Head {list::ListHandle v} | Nil
	deriving(Eq)

type Buckets v = Array Int (IORef (Slot v))

data HTable k v = TH
	{
	buckets	 :: IORef (Buckets v),
	n_elemens :: IORef Int,
	len_tab  :: IORef Int,
	fHash	 :: (k -> Int)
	}

fhash :: Int -> Int
fhash a = a

-- function to create a new Hash Table
newHash :: Int -> (k -> Int) -> IO (HTable k v)
newHash size fHash = do
	slots <- replicateM size (newIORef Nil)
	let arrayHash = listArray (0,size-1) slots
	newList >>= \x -> writeIORef (arrayHash ! 0) Head {list=x}
	buckets <- newIORef arrayHash
	n_elemens <- newIORef 0
	len_tab <- newIORef size
	return (TH buckets n_elemens len_tab fHash)


-- function to insert a new key val to Hash table ------------------------------------------------------------------------------
insert :: Eq v => HTable k v -> k -> v -> IO Bool
insert table@(TH buckets n_elemens len_tab fHash) key value = do
	size <- readIORef len_tab
	slots <- readIORef buckets
	nInsert <- readIORef n_elemens
	reverseKey <- reverseBits $ fHash key
	if (threshold nInsert size) then do
		guard <- readIORef $ slots ! hashfun (fHash key) size
		case guard of
			Head {list=listGuard} -> do
				tst <- addToList listGuard reverseKey value
				if tst then do
					incHashInsert n_elemens
					return tst
					else return tst
			Nil -> do
				newGuard <- initializeGuard slots size $ hashfun (fHash key) size
				tst <- addToList newGuard reverseKey value
				if tst then do
					incHashInsert n_elemens
					return tst
					else return tst
		else do
			newHash <- doubleTable slots size
			size2 <- readIORef len_tab
			if (size == size2) then do
				tst <- atomCAS buckets slots newHash
				if tst then do
					tst2 <- atomCAS len_tab size (size*2)
					insert table key value
					else do
						insert table key value
				else do
					insert table key value
-- ===========================================================================================================================
-- function to delete a element from list ------------------------------------------------------------------------------------
delete :: Eq k => Eq v => HTable k v -> k -> IO Bool
delete table@(TH buckets n_elemens len_tab fHash) key = do
	size <- readIORef len_tab
	slots <- readIORef buckets
	reverseKey <- reverseBits $ fHash key
	ptrList <- readIORef $ slots ! hashfun (fHash key) size
	case ptrList of
		Head {list=listGuard} -> do
			tst <- delFromList listGuard reverseKey
			if tst then do
				decHashInsert n_elemens
				return tst
				else return tst
		Nil -> do
			guard <- initializeGuard slots size $ hashfun (fHash key) size
			tst <- delFromList guard reverseKey
			if tst then do
				decHashInsert n_elemens
				return tst
				else return tst
-- ===========================================================================================================================
-- function to find a element in de hash table -------------------------------------------------------------------------------
contains :: Eq k => Eq v => HTable k v -> k -> IO (Maybe v)
contains table@(TH buckets n_elemens len_tab fHash) key = do
	size <- readIORef len_tab
	slots <- readIORef buckets
	reverseKey <- reverseBits $ fHash key
	ptrList <- readIORef $ slots ! hashfun (fHash key) size
	case ptrList of
		Head {list=listGuard} -> do
			tst <- findInList listGuard reverseKey
			return tst
		Nil -> do
			guard <- initializeGuard slots size $ hashfun (fHash key) size
			tst <- findInList guard reverseKey
			return tst
-- ============================================================================================================================
-- auxiliar functions ---------------------------------------------------------------------------------------------------------
hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab

foreign import ccall safe reverseBits :: Int -> IO Word

incHashInsert :: IORef Int -> IO Bool
incHashInsert var = do
	old <- readIORef var
	new <- let n = old+1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else incHashInsert var

decHashInsert :: IORef Int -> IO Bool
decHashInsert var = do
	old <- readIORef var
	new <- let n = old-1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else decHashInsert var

initializeGuard :: Eq v => Buckets v -> Int -> Int -> IO (ListHandle v)
initializeGuard slots size key = do
	x <- readIORef $ slots ! searchParent size key
	reverseKey <- reverseBits key
	case x of
		Head {list = listGuard} -> do
			guard <- addGuardToList listGuard reverseKey
			writeIORef (slots ! key) (Head {list=guard})
			return guard
		Nil -> do
			newGuard <- initializeGuard slots size $ searchParent size key
			guard <- addGuardToList newGuard reverseKey
			writeIORef (slots ! key) (Head {list=guard})
			return guard

searchParent :: Int -> Int -> Int
searchParent size endSlot | newPos > endSlot = searchParent newPos endSlot
						  | otherwise = endSlot - newPos
			where newPos = shiftR size 1

doubleTable :: Buckets v -> Int -> IO (Buckets v)
doubleTable slots size = do
	let oldList = elems slots
	x <- replicateM (size*2) $ newIORef Nil
	let newArrayHash = listArray (0,(size*2)-1) $ oldList ++ x
	return newArrayHash

threshold :: Int -> Int -> Bool
threshold n_elemens tamTab = fromIntegral n_elemens * 0.75 <= fromIntegral tamTab
