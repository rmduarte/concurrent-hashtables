{- Lista encadeada não bloqueante -}
module HashTable.ListCAS.ListCAS where
--module ListCAS.ListCAS where

import Data.IORef
-- import Data.CAS -- ghc 7.6.4
import Data.Atomics -- ghc 7.10.3
import Data.Word


data List v = Node {nodeVal::(Word,v), next :: IORef (List v)}
			| DelNode {next :: IORef (List v)}
			| Null
			| Guard {guardVal::Word, next :: IORef (List v)}
	deriving(Eq)

type  ListHandle v = IORef (List v)



atomCAS :: Eq a => IORef a -> a -> a -> IO Bool
atomCAS ptr old new =
	atomicModifyIORefCAS  ptr (\cur -> if cur == old
								then (new,True)
								else (cur,False))

-- function to create a new list
newList :: IO (ListHandle v)
newList = do
	guard <- (\x -> newIORef (Guard {guardVal=0,next=x})) =<< newIORef Null
	return guard

-- function to add new item in list
-- ==============================================================================================================================================================
addToList :: Eq v => ListHandle v -> Word -> v -> IO Bool
addToList list value = addList list list value

addList :: Eq v => ListHandle v -> ListHandle v -> Word -> v -> IO Bool
addList listPtr listPtrBk key value = do
	currNodeVal <- readIORef listPtr
	case currNodeVal of
		-- case of find a Node
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then do
						addList nextPtr listPtrBk key value
						else do
							newPtr <- newIORef Node {nodeVal=(key,value), next=nextPtr}
							tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=newPtr})
							if tst then return True else addList listPtrBk listPtrBk key value

				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=nextPtr2})
					if tst then return True else addList listPtrBk listPtrBk key value

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then return False
						else do
							if (nextKey > key) then do
								newPtr <- newIORef (Node {nodeVal=(currKey,currVal), next=nextPtr})
								tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal), next=newPtr})
								if tst then return True else addList listPtrBk listPtrBk key value
								else addList nextPtr listPtrBk key value

				Null -> do
					newPtr <- newIORef Node {nodeVal=(key,value), next=nextPtr}
					tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(key,value), next=newPtr})
					if tst then return True else addList listPtrBk listPtrBk key value
		-- case of find a Guard
		Guard {guardVal=currKey,next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then do
						addList nextPtr listPtrBk key value
						else do
							newPtr <- newIORef Node {nodeVal=(key,value),next=nextPtr}
							tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=newPtr})
							if tst then return True else addList listPtrBk listPtrBk key value
				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal Guard {guardVal=currKey,next=nextPtr2}
					if tst then addList listPtr listPtrBk key value else addList listPtrBk listPtrBk key value

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then
						return False
						else do
							if (nextKey > key) then do
								newPtr <- newIORef Node {nodeVal=(key,value), next=nextPtr2}
								tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=newPtr})
								if tst then return True else addList listPtrBk listPtrBk key value
								else addList nextPtr listPtrBk key value
				Null -> do
					newPtr <- newIORef Node {nodeVal=(key,value), next=nextPtr}
					tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=newPtr})
					if tst then return True else addList listPtrBk listPtrBk key value

		DelNode {next=nextPtr} -> addList nextPtr listPtrBk key value

		Null -> do
			newNext <- newIORef Null
			tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(key,value), next=newNext})
			if tst then return True else addList listPtrBk listPtrBk key value
-- ==============================================================================================================================================================
-- função que remove um valor da lista encadeada
delFromList :: Eq v => ListHandle v -> Word -> IO Bool
delFromList list key = delList list list key

delList :: Eq v => ListHandle v -> ListHandle v -> Word -> IO Bool
delList listPtr listPtrBk key = do
	currNodeVal <- readIORef listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then delList nextPtr listPtrBk key else return False

				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=nextPtr2})
					if tst then delList listPtr listPtrBk key else delList listPtrBk listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then do
						tst <- atomCAS nextPtr nextNodeVal (DelNode {next=nextPtr2})
						if tst then return True else delList listPtrBk listPtrBk key
						else do
							if (nextKey > key) then return False else delList nextPtr listPtrBk key
 				Null -> return False

		Guard {guardVal=currKey,next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then delList nextPtr listPtrBk key else return False

				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=nextPtr2})
					if tst then delList listPtr listPtrBk key else delList listPtrBk listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then do
						tst <- atomCAS nextPtr nextNodeVal (DelNode {next=nextPtr2})
						if tst then return True else delList listPtrBk listPtrBk key
						else do
							if (nextKey > key) then return False else delList nextPtr listPtrBk key

				Null -> return False

		DelNode {next=nextPtr} -> delList nextPtr listPtrBk key

		Null -> return False
-- ==================================================================================================================================================================
-- function to search a element from list
findInList :: Eq v => ListHandle v -> Word -> IO (Maybe v)
findInList list key = findList list list key

findList :: Eq v => ListHandle v -> ListHandle v -> Word -> IO (Maybe v)
findList listPtr listPtrBk key = do
	currNodeVal <- readIORef listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal),next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey <= key) then findList nextPtr listPtrBk key else return Nothing

				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=nextPtr2})
					if tst then findList listPtr listPtrBk key else findList listPtrBk listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then return $ Just nextVal else findList nextPtr listPtr key

				Null -> return Nothing

		Guard {guardVal=currKey, next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey, next=nextPtr2} -> do
					if (nextKey <= key) then findList nextPtr listPtrBk key else return Nothing

				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=nextPtr2})
					if tst then findList listPtr listPtrBk key else findList listPtrBk listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey == key) then return $ Just nextVal else findList nextPtr listPtrBk key

				Null -> return Nothing

		DelNode {next=nextPtr} -> findList nextPtr listPtrBk key

		Null -> return Nothing

-- ================================================================================================================================================================
--
-- ================================================================================================================================================================
addGuardToList :: Eq v => ListHandle v -> Word -> IO (ListHandle v)
addGuardToList list key = addGuard list list key

addGuard :: Eq v => ListHandle v -> ListHandle v -> Word -> IO (ListHandle v)
addGuard listPtr listPtrBk key = do
	currNodeVal <- readIORef listPtr
	case currNodeVal of
		Node {nodeVal=(currKey,currVal), next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey, next=nextPtr2} -> do
					if (nextKey == key) then return nextPtr
						else do
							if (nextKey > key) then do
								newGuard <- newIORef Guard {guardVal=key,next=nextPtr}
								tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=newGuard})
								if tst then
									return newGuard
									else addGuard listPtrBk listPtrBk key
							else addGuard nextPtr listPtrBk key

				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=nextPtr2})
					if tst then addGuard listPtr listPtrBk key else addGuard listPtrBk listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey >= key) then do
						newGuard <- newIORef Guard {guardVal=key,next=nextPtr}
						tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=newGuard})
						if tst then return newGuard else addGuard listPtrBk listPtrBk key
						else addGuard nextPtr listPtrBk key

				Null -> do
					newGuard <- newIORef Guard {guardVal=key,next=nextPtr}
					tst <- atomCAS listPtr currNodeVal (Node {nodeVal=(currKey,currVal),next=newGuard})
					if tst then return newGuard else addGuard listPtrBk listPtrBk key

		Guard {guardVal=currKey,next=nextPtr} -> do
			nextNodeVal <- readIORef nextPtr
			case nextNodeVal of
				Guard {guardVal=nextKey,next=nextPtr2} -> do
					if (nextKey == key) then return nextPtr
						else do
							if(nextKey > key) then do
								newGuard <- newIORef Guard {guardVal=key,next=nextPtr}
								tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=newGuard})
								if tst then return newGuard else addGuard listPtrBk listPtrBk key
								else addGuard nextPtr listPtrBk key


				DelNode {next=nextPtr2} -> do
					tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey, next=nextPtr2})
					if tst then addGuard listPtr listPtrBk key else addGuard listPtrBk listPtrBk key

				Node {nodeVal=(nextKey,nextVal),next=nextPtr2} -> do
					if (nextKey >= key) then do
						newGuard <- newIORef Guard {guardVal=key, next=nextPtr}
						tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=newGuard})
						if tst then return newGuard else addGuard listPtrBk listPtrBk key
						else addGuard nextPtr listPtrBk key

				Null -> do
					newGuard <- newIORef Guard {guardVal=key,next=nextPtr}
					tst <- atomCAS listPtr currNodeVal (Guard {guardVal=currKey,next=newGuard})
					if tst then return newGuard else addGuard listPtrBk listPtrBk key

		DelNode {next=nextPtr} -> addGuard nextPtr listPtrBk key

		Null -> do
			newNext <- newIORef Null
			tst <- atomCAS listPtr currNodeVal (Guard {guardVal=key,next=newNext})
			if tst then return listPtr else addGuard listPtrBk listPtrBk key
-- -- ================================================================================================================================================================
