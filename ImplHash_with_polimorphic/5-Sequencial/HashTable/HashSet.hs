module HashTable.HashSet where
import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.MVar

type Buckets k v = Array Int [(k,v)]

data HTable k v = TH
    {
    buckets :: Buckets k v,
    n_elements :: Int,
    len_tab :: Int,
    fHash :: k -> Int
    }

newHash :: Int -> (k -> Int) -> HTable k v
newHash initSize fHash = (TH (listArray (0,initSize-1) $ replicate initSize []) 0 initSize fHash)

insert :: Eq k => Eq v => HTable k v -> k -> v -> (HTable k v,Bool)
insert table@(TH buckets n_elements len_tab fHash) key value = case (lookup key l) of
                                                                 Just a -> (table, False)
                                                                 Nothing -> if not(threshold n_elements len_tab) then insert (TH newBuckets n_elements (len_tab*2) fHash) key value
                                                                                             else ((TH (buckets // [(pos,(key,value):l)]) (n_elements+1) len_tab fHash),True)
                         where pos = hashfun (fHash key) len_tab;
                                 l = buckets ! pos;
                                 newBuckets = resize buckets len_tab fHash



contains :: Eq k => Eq v => HTable k v -> k -> Maybe v
contains table@(TH buckets n_elements len_tab fHash) key = lookup key $ buckets ! hashfun (fHash key) len_tab

delete :: Eq k => Eq v => HTable k v -> k -> (HTable k v, Bool)
delete table@(TH buckets n_elements len_tab fHash) key = case (lookup key l) of
                                                                Just a -> (TH (buckets // [(pos,filter (\x -> key /= fst x) l)]) (n_elements-1) len_tab fHash,True)
                                                                Nothing -> (table,False)
                        where pos = hashfun (fHash key) len_tab;
                                l = buckets ! pos


hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab

threshold :: Int -> Int -> Bool
threshold n_elements tamTab = fromIntegral n_elements * 0.75 <= fromIntegral tamTab

-- ---- ---------------------------------------------------------------------
resize :: Buckets k v -> Int -> (k -> Int) -> Buckets k v
resize old_array old_size fHash = resizeArray old_array (listArray (0,(old_size*2)-1) (replicate (old_size*2) [])) (old_size) (old_size*2) fHash

resizeArray :: Buckets k v -> Buckets k v -> Int -> Int -> (k -> Int) -> Buckets k v
resizeArray array1 array2 0 new_size fHash = array2
resizeArray array1 array2 old_size new_size fHash = resizeArray array1 (copy_list l array2 new_size fHash) (old_size-1) new_size fHash
          where l = array1 ! (old_size-1)

copy_list :: [(k,v)] -> Buckets k v -> Int -> (k -> Int) -> Buckets k v
copy_list [] array1 size_array fHash = array1
copy_list ((key,value):xs) array1 size_array fHash = copy_list xs (array1 // [(pos,(key,value):l)]) size_array fHash
  where pos = hashfun (fHash key) size_array;
          l = array1 ! pos
