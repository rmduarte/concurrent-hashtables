module Main where

import HashTable.HashSet
import Control.Concurrent
import GHC.Conc
import Control.Monad
import System.Random
import System.Time
import Text.Printf
import System.Environment

insertInHash :: Int -> Int -> HTable Int Int -> IO (HTable Int Int)
insertInHash 0 _ ourHashTable = return ourHashTable
insertInHash numOps maxNumber ourHashTable = do
	rnd1 <- randomRIO (1::Int, 10)
	rnd2 <- randomRIO (1::Int, maxNumber)
	case rnd1 of
		1 -> insertInHash (numOps-1) maxNumber $ (fst $ delete ourHashTable rnd2)
		2 -> do return (contains ourHashTable rnd2) >> insertInHash (numOps-1) maxNumber ourHashTable
		_ -> insertInHash (numOps-1) maxNumber $ (fst $ insert ourHashTable rnd2 rnd2)

main1 :: Int -> Int -> IO ()
main1 numops keysRange = do
	let ourHashTable = newHash 16 (\x -> x)
	timeStart <- getClockTime
	print(numops)
	insertInHash numops keysRange ourHashTable
	timeEnd <- getClockTime
	let diff = (normalizeTimeDiff (diffClockTimes timeEnd timeStart))
	print ("#"++show((((fromIntegral(tdPicosec diff))/(10^12))) + (fromIntegral((tdSec diff) + (60 * (tdMin diff)) + (3600 * (tdHour diff))))))
	return ()

callNTimes :: Int -> IO () -> IO ()
callNTimes 0 _ = return ()
callNTimes times f = do
	f
	callNTimes (times-1) f

main :: IO ()
main = do
 	args <- getArgs
	let numops = read (args !! 0)
	let keysrange = read (args !! 1)
	let numiterations = read (args !! 2)
	callNTimes numiterations (main1 numops keysrange)
