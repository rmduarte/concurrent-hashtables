module HashTable.HashSet where
import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.MVar

type Buckets k v = Array Int [(k,v)]

data Table k v = T
    {
        buckets :: Buckets k v,
        n_elemens :: Int,
        len_tab :: Int,
        fHash :: k -> Int
    }

data HTable k v = TH {table :: MVar (Table k v)}

fhash :: Int -> Int
fhash a = a

newHash :: Int -> (k -> Int) -> IO (HTable k v)
newHash initSize fHash = do
    let slots = replicate initSize []
    buckets <- let vector = listArray(0,initSize-1) slots in vector `seq` return vector
    table <- newMVar $ T buckets 0 initSize fHash
    return (TH table)

insert :: Eq k => Eq v => HTable k v -> k -> v -> IO Bool
insert tb@(TH table) key value = do
    hash@(T buckets n_elemens len_tab fHash) <- takeMVar table
    if(threshold n_elemens len_tab) then do
        let pos = hashfun (fHash key) len_tab
        let l = buckets ! pos
        case (lookup key l) of
            Just a -> putMVar table hash >> return False
            Nothing -> do
                newBuckets <- let vector = buckets // [(pos,(key,value):l)] in vector `seq` return vector
                putMVar table (T newBuckets (n_elemens+1) len_tab fHash)
                return True
        else do
            putMVar table (T (resize buckets len_tab fHash) n_elemens (len_tab*2) fHash)
            insert tb key value

contains :: Eq k => HTable k v -> k -> IO (Maybe v)
contains tb@(TH table) key = do
    hash@(T buckets n_elemens len_tab fHash) <- takeMVar table
    let l = buckets ! hashfun (fHash key) len_tab
    putMVar table hash
    return $ lookup key l

delete :: Eq k => Eq v => HTable k v -> k -> IO Bool
delete tb@(TH table) key = do
    hash@(T buckets n_elemens len_tab fHash) <- takeMVar table
    let pos = hashfun (fHash key) len_tab
    let l = buckets ! pos
    case (lookup key l) of
        Just a -> do
            newBuckets <- let vector = buckets // [(pos,filter (\x -> key /= fst x) l)] in vector `seq` return vector 
            putMVar table (T newBuckets (n_elemens-1) len_tab fHash)
            return True
        Nothing -> putMVar table hash >> return False
	
hashfun :: Int -> Int -> Int
hashfun val tam_tab = mod val tam_tab
-- --
threshold :: Int -> Int -> Bool
threshold n_elemens tamTab = fromIntegral n_elemens * 0.75 <= fromIntegral tamTab

---- ---------------------------------------------------------------------

resize :: Buckets k v -> Int -> (k -> Int) -> Buckets k v
resize old_array old_size fHash = resizeArray old_array (listArray (0,(old_size*2)-1) (replicate (old_size*2) [])) (old_size) (old_size*2) fHash

resizeArray :: Buckets k v -> Buckets k v -> Int -> Int -> (k -> Int) -> Buckets k v
resizeArray array1 array2 0 new_size fHash = array2
resizeArray array1 array2 old_size new_size fHash = resizeArray array1 (copy_list l array2 new_size fHash) (old_size-1) new_size fHash
          where l = array1 ! (old_size-1)

copy_list :: [(k,v)] -> Buckets k v -> Int -> (k -> Int) -> Buckets k v
copy_list [] array1 size_array fHash = array1
copy_list ((key,value):xs) array1 size_array fHash = copy_list xs (array1 // [(pos,(key,value):l)]) size_array fHash
  where pos = hashfun (fHash key) size_array;
          l = array1 ! pos