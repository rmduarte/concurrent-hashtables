module HashTable.HashSet where

import Data.Array
import Data.IORef
import Control.Monad
import Control.Concurrent.MVar
-- import Data.CAS -- ghc 7.6.4
import Data.Atomics -- ghc 7.10.3


type Buckets k v = Array Int (IORef [(k,v)])
type Locks = Array Int (MVar ())

data HTable k v = TH
	{
	buckets	  :: IORef (Buckets k v),
	locks	  :: Locks,
	n_insert  :: IORef Int,
	sizeTab	  :: IORef Int,
	sizeLocks :: Int,
	fHash     :: k -> Int
	}

fhash :: Int -> Int
fhash a = a

newHash :: Int -> (k -> Int) -> IO (HTable k v)
newHash initSize fHash = do
	slots <- replicateM initSize (newIORef [])
	array_locks <- replicateM initSize (newMVar ())
	buckets <- newIORef =<< let vector' = listArray (0,initSize-1) slots in vector' `seq` return vector'
	locks <- let locks' = listArray (0,(initSize-1)) array_locks in locks' `seq` return locks'
	n_insert <- newIORef 0
	sizeTab <- newIORef initSize
	return (TH buckets locks n_insert sizeTab initSize fHash)
--
-- -- --------------------- função de inserção do valor ------------------------------
--
insert :: Eq k => Eq v => HTable k v -> k -> v -> IO Bool
insert table@(TH buckets locks n_insert sizeTab sizeLocks fHash) key value = do
	let lock = locks ! hashfun sizeLocks (fHash key)	-- seleciona o lock correspondente
	lockAcquire lock
	slots <- readIORef buckets
	size <- readIORef sizeTab
	nInsert <- readIORef n_insert
	l <- readIORef $ slots ! hashfun size (fHash key) 	-- busca a lista correspondente na tabela
	case lookup key l of
		Just a -> lockRelease lock >> return False
		Nothing -> do
			if (threshold nInsert size) then do
				writeIORef (slots ! hashfun size (fHash key)) ((key,value):l)
				incHashInsert n_insert
				lockRelease lock
				return True
				else do
					lockRelease lock
					ok <- acquireAllLocks locks sizeLocks size sizeTab
					if ok then do
						newBuckets <- resize slots size fHash
						writeIORef buckets newBuckets
						writeIORef sizeTab (size*2)
						releaseAllLocks locks sizeLocks
						insert table key value
						else do
							insert table key value

-- -------------------- função de consulta do valor ------------------------------

contains :: Eq k => Eq v => HTable k v -> k -> IO (Maybe v)
contains table@(TH buckets locks n_insert sizeTab sizeLocks fHash) key = do
	let lock = locks ! hashfun sizeLocks (fHash key)	-- seleciona o lock correspondente
	lockAcquire lock
	slots <- readIORef buckets
	size <- readIORef sizeTab
	nInsert <- readIORef n_insert
	l <- readIORef $ slots ! hashfun size (fHash key) 	-- busca a lista correspondente na tabela
	lockRelease lock
	return $ lookup key l
-- -------------------------------------------------------------------------------

-- ------------------- função de remoção do valor --------------------------------
delete :: Eq k => Eq v => HTable k v -> k -> IO (Bool)
delete table@(TH buckets locks n_insert sizeTab sizeLocks fHash) key = do
	let lock = locks ! hashfun sizeLocks (fHash key)	-- seleciona o lock correspondente
	lockAcquire lock
	slots <- readIORef buckets
	size <- readIORef sizeTab
	nInsert <- readIORef n_insert
	l <- readIORef $ slots ! hashfun size (fHash key) 	-- busca a lista correspondente na tabela
	case lookup key l of
		Just a -> do
			nl <- let l' = filter (\x -> key /= fst x) l in l' `seq` return l'
			writeIORef (slots ! hashfun size (fHash key)) nl
			decHashInsert n_insert
			lockRelease lock
			return True
		Nothing -> do
			lockRelease lock
			return False

-- -- ---------------- Funções auxiliares -------------------------------
hashfun :: Int -> Int -> Int
hashfun sizeTab key = mod key sizeTab

lockAcquire :: MVar () -> IO ()
lockAcquire lock = do
	x <- tryTakeMVar lock
	case x of
		Just a -> return ()
		Nothing -> lockAcquire lock

lockRelease :: MVar () -> IO ()
lockRelease lock = putMVar lock ()

acquireAllLocks :: Locks -> Int -> Int -> IORef Int -> IO Bool
acquireAllLocks locks sizeLocks oldSize ptrSizeTab = do
	x <- tryTakeMVar $ locks ! (sizeLocks-1)
	case x of
		Just a -> acquireAllLocks' locks (sizeLocks-1)
		Nothing -> do
			newSize <- readIORef ptrSizeTab
			if oldSize == newSize then
				acquireAllLocks locks sizeLocks oldSize ptrSizeTab
				else return False


acquireAllLocks' :: Locks -> Int -> IO Bool
acquireAllLocks' locks 0 = return True
acquireAllLocks' locks qtdLocks = do
	x <- tryTakeMVar $ locks ! (qtdLocks-1) 				-- tenta adquirir o lock
	case x of
		Just a -> acquireAllLocks' locks (qtdLocks-1) 		-- se falhar, tenta novamente
		Nothing -> acquireAllLocks' locks (qtdLocks)		-- se conseguir, vai para o proximo lock, tem de ser pego em ordem para evitar DL

releaseAllLocks :: Locks -> Int -> IO ()
releaseAllLocks locks 0 = return ()
releaseAllLocks locks qtdLocks = do
	putMVar (locks ! (qtdLocks - 1)) ()
	releaseAllLocks locks $ qtdLocks - 1


atomCAS :: Eq a => IORef a -> a -> a -> IO Bool
atomCAS ptr old new =
   atomicModifyIORefCAS ptr (\ cur -> if cur == old
                                   then (new, True)
                                   else (cur, False))
--
incHashInsert :: IORef Int -> IO Bool
incHashInsert var = do
	old <- readIORef var
	new <- let n = old+1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else incHashInsert var

decHashInsert :: IORef Int -> IO Bool
decHashInsert var = do
	old <- readIORef var
	new <- let n = old-1 in n `seq` return n
	ok <- atomCAS var old new
	if ok then return True else decHashInsert var

-- -------------------------------------------------------------------------------------------

threshold :: Int -> Int -> Bool
threshold n_insert tamTab = fromIntegral n_insert * 0.75 <= fromIntegral tamTab
--
copy_list :: [(k,v)] -> Buckets k v -> Int -> (k -> Int) -> IO Bool
copy_list [] newArray new_size fHash = return True
copy_list ((key,value):xs) newArray new_size fHash = do
	let array_pos = hashfun new_size (fHash key)
	list <- readIORef (newArray ! array_pos)
	newList <- let nl = (key,value):list in nl `seq` return nl
	writeIORef (newArray ! array_pos) newList
	copy_list xs newArray new_size fHash

resizeArray :: Buckets k v -> Buckets k v -> Int -> Int -> (k-> Int) -> IO (Buckets k v)
resizeArray oldArray newArray 0 new_size fHash = return newArray
resizeArray oldArray newArray old_size new_size fHash = do
	list <- readIORef $ oldArray ! (old_size-1)
	copy_list list newArray new_size fHash
	resizeArray oldArray newArray (old_size-1) new_size fHash

resize :: Buckets k v -> Int -> (k-> Int) -> IO (Buckets k v)
resize oldArray old_size fHash = do
	buckets <- replicateM (old_size*2) (newIORef [])
	newArray <- let arrayHash' = listArray (0,(old_size*2)-1) buckets in arrayHash' `seq` return arrayHash'
	new_array <- resizeArray oldArray newArray old_size (old_size*2) fHash
	return new_array
-- -- ---------------Implementação do atomCAS ----------------------
