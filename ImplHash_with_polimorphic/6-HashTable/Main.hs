 module Main where

 import Data.THash
 import Control.Concurrent
 import GHC.Conc
 import Control.Monad
 import System.Random 
 import System.Time
 import Text.Printf
 import System.Environment

 
 createThread :: Int -> THash Int Int -> Int -> MVar Integer -> IO ThreadId
 createThread numOps ourHashTable maxNumber mvar =
 	 forkIO ( do
			{ callNTimes numOps 
				(do 
				{ rnd1 <- randomRIO (1::Int, 10)
				; rnd2 <- randomRIO (1::Int, maxNumber)

				; case rnd1 of
				; 1 -> do {atomically (delete ourHashTable rnd2)
									; return ()}
				; 2 -> do {atomically (insert ourHashTable rnd2 rnd2)
									; return ()}
				; otherwise -> do {atomically (Data.THash.lookup ourHashTable rnd2)
									; return ()}
				}) 
			; putMVar mvar 1
			}
          	 )



 createThreads n numOps tHash maxNumber mvars = mapM_ (createThread numOps tHash maxNumber) mvars

 main1 numops keysRange numThreads = do
 	ourHashTable <- atomically (new hashInt)
	timeStart <- getClockTime
	mvars <- replicateM numThreads newEmptyMVar
	threads <- createThreads numThreads numops ourHashTable keysRange mvars
	mapM_ takeMVar mvars
	timeEnd <- getClockTime
	let diff = (normalizeTimeDiff (diffClockTimes timeEnd timeStart))
	print ((((fromIntegral(tdPicosec diff))/(10^12))) + (fromIntegral((tdSec diff) + (60 * (tdMin diff)) + (3600 * (tdHour diff)))))
	return ()


 callNTimes 0 _ = return ()
 callNTimes times f = do
 			f 
			callNTimes (times-1) f


 callNTimesSTM 0 _ = return ()
 callNTimesSTM times f = do
 			f 
			callNTimesSTM (times-1) f

 main :: IO()
 main = do
  	args <- getArgs
	let numops = read (args!!0)
	let keysrange = read (args!!1)
	let numthreads = read (args!!2)
	let numiterations = read (args!!3)
	callNTimes numiterations (main1 numops keysrange numthreads)